<?php include_once('connection_db.php'); ?>
<?php include("layouts/header.php") ?>

<div class="w3-content w3-section mt-5" style="max-width:1920px">
  <img class="mySlides" src="assets/images/slide1.jpeg" style="width:100%">
  <img class="mySlides" src="assets/images/slide2.jpeg" style="width:100%">
  <img class="mySlides" src="assets/images/slide3.jpeg" style="width:100%">
</div>

<div class="container">
  <section class="mt-4 mb-5">
    <h3>Les produits</h3>

    <div class="row">
    <?php
        $get_all_produits = "SELECT * FROM `produits` WHERE id BETWEEN 9 AND 12";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
            while ($rs = mysqli_fetch_assoc($res)) {
                ?>

                <div class="col-md-3 mb-4">
                    <div class="card">
                        <img class="card-img-top"
                            src="assets/images/produits/<?php echo $rs['image']; ?>"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo $rs['nom']; ?>
                            </h5>
                            <p class="card-text">
                                <?php echo $rs['description']; ?>
                            </p>
                            <strong>
                                Prix: <?php echo $rs['prix1']?? "_"; ?> Dh
                            </strong>
                        </div>
                    </div>
                </div>

                <?php
            }
        } else {
            ?>
        <p>Pas de données ...</p>
        <?php
        }
        ?>

        <p>
           Voulez-vous acheter nos produits ?
           <a href="pages/produits/client_produits.php">
            cliquez ici .
          </a>
        </p>
    </div>
  </section>

  <section class="mt-4 mb-5">
    <h3>Les 4 produits plus vendu</h3>

    <div class="row">
    <?php
        $get_all_produits = "SELECT produit_id,p.nom, p.description, p.prix1, p.image, p.stock, SUM(qte) as qte,v.nom_complet as vendeur FROM detail_panier dp, produits p, vendeurs v WHERE p.id = dp.produit_id AND v.id = p.id_vendeur GROUP BY produit_id ORDER BY qte DESC LIMIT 4;";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
            while ($rs = mysqli_fetch_assoc($res)) {
                ?>

                <div class="col-md-3 mb-4">
                    <div class="card">
                        <img class="card-img-top"
                            src="assets/images/produits/<?php echo $rs['image']; ?>"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo $rs['nom']; ?>
                            </h5>
                            <p class="card-text">
                                <?php echo $rs['description']; ?>
                            </p>
                            <strong>
                                Prix: <?php echo $rs['prix1']?? "_"; ?> Dh
                            </strong>
                            
                        </div>
                    </div>
                </div>

                <?php
            }
        } else {
            ?>
        <p>Pas de données ...</p>
        <?php
        }
        ?>

        <p>
           Voulez-vous acheter nos produits ?
            <a href="pages/produits/client_produits.php">
               cliquez ici .
            </a>
        </p>
    </div>
  </section>

</div>

<script>
  var myIndex = 0;
  carousel();

  function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) { myIndex = 1 }
    x[myIndex - 1].style.display = "block";
    setTimeout(carousel, 1000); // Change image every 1 second
  }
</script>

<!-- Contenu principal -->

<!-- Pied de page -->
<?php include("layouts/footer.php") ?>