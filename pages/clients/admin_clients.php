<?php include_once('../../connection_db.php'); ?>

<?php include("../../layouts/admin/header.php") ?>


    <!-- Main -->
    <main class="main-container">
        <h3 class="mb-3">Clients</h3>

        <table class="table shadow-sm p-3 mb-5 bg-white">
      <thead>
        <tr>
          <th>Nom complet</th>
          <th>Telephone</th>
          <th>Adresse</th>
          <th>Pays</th>
          <th>Ville</th>
          <th>e-mail</th>
          <th>Etat compte</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $get_all_produits = "SELECT * FROM clients";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
            <tr>
              <td>
                <?php echo $rs['nom_complet']; ?>
              </td>
              <td>
                <?php echo $rs['telephone']; ?>
              </td>
              <td>
                <?php echo $rs['adresse']; ?>
              </td>

              <td>
                <?php echo $rs['pays']; ?>
              </td>
              <td>
                <?php echo $rs['ville']; ?>
              </td>
              <td>
                <?php echo $rs['email']; ?>
              </td>
              <td>
                <?php echo($rs['compte_active'] == true ? "Oui":"Non"); ?>
              </td>
             
            </tr>
            <?php
          }
        } else {
          ?>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Pas de données ...</p>
          </td>
        </tr>
        <?php
        }
        ?>


      </tbody>
    </table>
    </main>
    <!-- End Main -->

<?php include("../../layouts/admin/footer.php") ?>