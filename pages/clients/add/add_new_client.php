<?php include("../../../layouts/admin/header.php") ?>
<?php include_once('../../../connection_db.php'); ?>

    <main class="main-container">
    
     	<!-- <div class="container"> -->
     		<div class="row">
     			<div class="col-md-7 form_add_pro">
    			    <h3 class="text-center pt-2">Nouveau client</h3>
    
     				<form action="insert_client.php" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white">
     					<div class="form-group">
     						<label>Nom complet:</label>
     						<input type="text" name="nom_complet" class="form-control" placeholder="nom_complet">
     					</div>
						 <div class="form-group">
     						<label>Telephone:</label>
     						<input type="text" name="telephone" class="form-control" placeholder="telephone">
     					</div>
    					 <div class="form-group">
     						<label>Adresse:</label>
     						<textarea name="adresse" class="form-control" placeholder="adresse" rows="3"></textarea>
     					</div>
     					<div class="form-group">
     						<label>Pays:</label>
     						<input type="text" name="pays" class="form-control" placeholder="pays">
     					</div>
                        <div class="form-group">
     						<label>ville:</label>
     						<input type="text" name="ville" class="form-control" placeholder="ville">
     					</div>
						 

						<div class="form-group">
     						<label>E-mail:</label>
     						<input type="email" name="email" class="form-control" placeholder="email">
     					</div>
						 <div class="form-group">
     						<label>Mot de passe:</label>
     						<input type="text" name="mdp" class="form-control" placeholder="mdp">
     					</div>
						 
     					<input type="submit" name="submit" class="btn btn-warning mt-3">
     				</form>
     			</div>
     		</div>
     	<!-- </div> -->
    
    </main>
 
 <?php include("../../../layouts/admin/footer.php") ?>