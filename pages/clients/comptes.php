<?php include_once('../../connection_db.php'); ?>
<?php include("../../layouts/admin/header.php") ?>

<?php
// Variable pour afficher le message de succès
$message = "";

// Traitement de la soumission du formulaire
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupération des valeurs du formulaire
    $compte = $_POST['compte'];
    $activation = $_POST['activation'];

    // Mise à jour du compte dans la base de données
    $query = "";

    if ($activation === 'activer') {
        $query = "UPDATE clients SET compte_active = 1 WHERE id = $compte";
    } else if ($activation === 'desactiver') {
        $query = "UPDATE clients SET compte_active = 0 WHERE id = $compte";
    }

    // Exécution de la requête de mise à jour
    if (!empty($query)) {
        $result = mysqli_query($conn, $query);
        if (!$result) {
            echo "Erreur de mise à jour du compte : " . mysqli_error($conn);
        } else {
            $message = "Le compte a été mis à jour avec succès.";
        }
    }
}
?>
<main class="main-container">
    <div class="content-container">
        <div class="container" id="container">
            <h2>Les comptes des utilisateurs</h2>
            <form action="comptes.php" method="post" class="_form">
                <div class="form-group">
                    <label for="compte">Sélectionnez un compte :</label>
                    <select name="compte" id="compte" class="form-select">
                        <option></option>
                        <?php
                        // Sélection des comptes de la table "clients" et "vendeurs"
                        $query = "SELECT id, nom_complet FROM clients UNION SELECT id, nom_complet FROM vendeurs";
                        $result = mysqli_query($conn, $query);

                        if (mysqli_num_rows($result) > 0) {
                            while ($row = mysqli_fetch_assoc($result)) {
                                echo "<option value='" . $row['id'] . "'>" . $row['nom_complet'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <!-- <br> -->
                <div class="form-group">
                    <label for="activation">Activer ou désactiver le compte :</label>
                    <select name="activation"  class="form-select"  id="activation">
                        <option></option>
                        <option value="activer">Activer</option>
                        <option value="desactiver">Désactiver</option>
                    </select>
                </div>
                <br>
                <input type="submit" value="Soumettre">
            </form>
        </div>
        <div id="message" class="message"></div>
    </div>

</main>

<?php if (!empty($message)) { ?>
    <script>
        // Afficher le message de succès et le faire disparaître après 5 secondes
        var messageElement = document.getElementById('message');
        messageElement.innerText = '<?php echo $message; ?>';
        messageElement.style.display = 'block';
        setTimeout(function () {
            messageElement.style.display = 'none';
        }, 3000);
    </script>
<?php } ?>



<?php include("../../layouts/admin/footer.php") ?>