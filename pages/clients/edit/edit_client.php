<?php include("../../../layouts/header.php") ?>
<?php include_once('../../../connection_db.php'); ?>

<?php
    // Initialize the session
    session_start();
     
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: ../../../auth/inscri-conn.php");
        exit;
    }

   $get_id = $_REQUEST['id'];
   $get_item = "SELECT * FROM clients WHERE id = ".$get_id;
   $res = mysqli_query($conn,$get_item);
   if(mysqli_num_rows($res)>0){
	 while($rs = mysqli_fetch_assoc($res)){
	   ?>

<main class="container" style="margin-top: 80px;">
    
     		<div class="row">
     			<div class="col-md-7 form_add_pro" style="display: block; margin: auto;">
    			    <h3 class="text-center pt-2">Mon profile</h3>
    
     				<form action="update_client.php?id=<?php echo $get_id;?>" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white">
     					<div class="form-group">
     						<label>nom_complet:</label>
     						<input type="text" name="nom_complet" class="form-control" placeholder="nom_complet" value='<?php echo $rs['nom_complet']; ?>'>
     					</div>
    					
     					<div class="form-group">
     						<label>telephone:</label>
     						<input type="text" name="telephone" class="form-control" placeholder="telephone" value='<?php echo $rs['telephone']; ?>'>
     					</div>
						 <div class="form-group">
     						<label>adresse:</label>
     						<textarea name="adresse" class="form-control" placeholder="adresse" rows="3"><?php echo $rs['adresse']; ?></textarea>
     					</div>
                        <div class="form-group">
     						<label>pays:</label>
     						<input type="text" name="pays" class="form-control" placeholder="pays" value='<?php echo $rs['pays']; ?>'>
     					</div>

						 <div class="form-group">
     						<label>ville:</label>
     						<input type="text" name="ville" class="form-control" placeholder="ville" value='<?php echo $rs['ville']; ?>'>
     					</div>
						 <div class="form-group">
     						<label>email:</label>
     						<input type="email" name="email" class="form-control" placeholder="email" value='<?php echo $rs['email']; ?>'>
     					</div>
						 <div class="form-group">
     						<label>mdp:</label>
     						<input type="text" name="mdp" class="form-control" placeholder="mdp" value='<?php echo $rs['mdp']; ?>'>
     					</div>

     					<input type="submit" name="submit" class="btn btn-warning mt-3" value="Enregistrer">
     				</form>
     			</div>
     		</div>
    
    </main>

<?php
	  }
	}
?>


<?php include("../../../layouts/footer.php") ?>