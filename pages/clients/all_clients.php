<?php include_once('../../connection_db.php'); ?>

<?php include("../../layouts/admin/header.php") ?>


    <!-- Main -->
    <main class="main-container">
        <h3 class="mb-3">Utilisateur</h3>
        <a href="add/add_new_client.php" class="btn btn-warning mb-2 pull-right mb-3">Nouveau Utilisateur</a>

        <table class="table shadow-sm p-3 mb-5 bg-white">
      <thead>
        <tr>
          <th>Nom_complet</th>
          <th>telephone</th>
          <th>adresse</th>

          <th>pays</th>
          <th>ville</th>
          <th>email</th>

          <th>compte_active</th>

          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $get_all_produits = "SELECT * FROM clients";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
            <tr>
              <td>
                <?php echo $rs['nom_complet']; ?>
              </td>
              <td>
                <?php echo $rs['telephone']; ?>
              </td>
              <td>
                <?php echo $rs['adresse']; ?>
              </td>

              <td>
                <?php echo $rs['pays']; ?>
              </td>
              <td>
                <?php echo $rs['ville']; ?>
              </td>
              <td>
                <?php echo $rs['email']; ?>
              </td>
              <td>
                <?php echo($rs['compte_active'] == true ? "Oui":"Non"); ?>
              </td>
              <td>
                <a href="edit/edit_client.php?id=<?php echo $rs['id']; ?>" class="btn btn-warning"> 
                    <i class="fa-solid fa-pen"></i>
                </a>
            </td>
            </tr>
            <?php
          }
        } else {
          ?>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Pas de données ...</p>
          </td>
        </tr>
        <?php
        }
        ?>


      </tbody>
    </table>
    </main>
    <!-- End Main -->

<?php include("../../layouts/admin/footer.php") ?>