<?php
include_once('../../connection_db.php');

$query = $_REQUEST['query'];
?>

<?php include("../../layouts/header.php") ?>

<!-- Main -->
<main class="container" style="margin-top: 100px;">
    <h3 class="mb-3">Produits</h3>
    <div class="row">
        <?php
        $get_all_produits = "SELECT p.*, v.nom_complet as nom_ven, c.nom as nom_cat
                              FROM produits p, vendeurs v, categories c
                              WHERE p.id_vendeur = v.id AND p.categorie_id = c.id AND p.nom like '%".$query."%'";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
            while ($rs = mysqli_fetch_assoc($res)) {
                ?>

                <div class="col-md-4 mb-4">
                    <div class="card">
                        <img class="card-img-top"
                            src="../../assets/images/produits/<?php echo $rs['image']; ?>"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo $rs['nom']; ?>
                            </h5>
                            <p class="card-text">
                                <?php echo $rs['description']; ?>
                            </p>
                            <strong> Prix: <?php echo $rs['prix2']?? "_"; ?> Dh</strong><br>
                            <a class="btn btn-sm btn-warning mt-2" href="../panier/panier.php?action=ajout&amp;id=<?php echo $rs['id']; ?>&amp;l=<?php echo $rs['nom']; ?>&amp;q=1&amp;p=<?php echo $rs['prix2']; ?>">
                                <i class="fa-solid fa-plus"></i> Ajouter au panier
                            </a>
                            <a class="btn btn-sm btn-warning mt-2"
                                href="detail_produit.php?id=<?php echo $rs['id']; ?>">
                                Fiche produit
                            </a>
                        </div>
                    </div>
                </div>
        <?php
            }
            } else {
        ?>
        <p>Pas de données ...</p>
        <?php
        }
        ?>
    </div>


    </tbody>
    </table>
</main>
<!-- End Main -->

<?php include("../../layouts/footer.php") ?>