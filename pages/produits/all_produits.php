<?php 
   include_once('../../connection_db.php');
   session_start();
   $id_vendeur = $_SESSION['vendeur_id'];
?>
<?php include("../../layouts/vendeur/header.php") ?>

    <!-- Main -->
    <main class="main-container">
        <h3 class="mb-3">Produit</h3>
        <a href="add/add_new_produit.php" class="btn btn-warning mb-2 pull-right mb-3">Nouveau produit</a>

        <table class="table shadow-sm p-3 mb-5 bg-white">
          <thead>
            <tr>
              <th>Nom</th>
              <th>Prix</th>
              <th>Categorie</th>
              <th>Stock</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
        <?php
        $get_all_produits = "SELECT p.*, v.nom_complet as nom_ven, c.nom as nom_cat
                              FROM produits p, vendeurs v, categories c
                              WHERE p.id_vendeur = v.id AND p.categorie_id = c.id AND v.id = $id_vendeur";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
            <tr>
              <td>
                <?php echo $rs['nom']; ?>
              </td>
              <td>
                <?php echo $rs['prix1']; ?>
              </td>
              <td>
                <?php echo $rs['nom_cat']; ?>
              </td>
              <td>
                <?php echo $rs['stock']; ?>
              </td>
              
              <td>
                
                <form action="delete_produit.php" method="post">
                  <a href="edit/edit_produit.php?id=<?php echo $rs['id']; ?>" class="btn btn-warning"> 
                      <i class="fa-solid fa-pen"></i>
                  </a>

                  <input type="hidden" name="id" value="<?php echo $rs['id']; ?>">
                  <button type="submit" name="submit" class="btn btn-danger">
                    <i class="fa-solid fa-trash"></i>
                  </button>
                </form>
            </td>
            </tr>
            <?php
          }
        } else {
          ?>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Pas de données ...</p>
          </td>
        </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
    
    </main>
    <!-- End Main -->

<?php include("../../layouts/vendeur/footer.php") ?>