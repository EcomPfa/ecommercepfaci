<?php include_once('../../connection_db.php'); ?>
<?php include("../../layouts/admin/header.php") ?>

    <!-- Main -->
    <main class="main-container">
        <h3 class="mb-3">Produits</h3>

        <table class="table shadow-sm p-3 mb-5 bg-white">
      <thead>
        <tr>
          <th>Nom</th>
          <th>Prix d'achat</th>
          <th>Prix de vente</th>
          <th>Categorie</th>
          <th>Vendeur</th>
          <th>Stock</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $get_all_produits = "SELECT p.*, v.nom_complet as nom_ven, c.nom as nom_cat
                              FROM produits p, vendeurs v, categories c
                              WHERE p.id_vendeur = v.id AND p.categorie_id = c.id";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
            <tr>
              <td>
                <?php echo $rs['nom']; ?>
              </td>
              <td>
                <?php echo $rs['prix1']; ?>
              </td>
              <td>
                <?php echo $rs['prix2']; ?>
              </td>

              <td>
                <?php echo $rs['nom_cat']; ?>
              </td>
              <td>
                <?php echo $rs['nom_ven']; ?>
              </td>
              <td>
                <?php echo $rs['stock']; ?>
              </td>
            </tr>
            <?php
          }
        } else {
          ?>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Pas de données ...</p>
          </td>
        </tr>
        <?php
        }
        ?>


      </tbody>
    </table>
    </main>
    <!-- End Main -->

<?php include("../../layouts/admin/footer.php") ?>