<?php include("../../../layouts/vendeur/header.php") ?>
<?php include_once('../../../connection_db.php'); ?>

<?php

   $get_id = $_REQUEST['id'];
   $get_item = "SELECT * FROM produits WHERE id = ".$get_id;
   $res = mysqli_query($conn,$get_item);
   if(mysqli_num_rows($res)>0){
	 while($rs = mysqli_fetch_assoc($res)){
	   ?>

        <main class="main-container">
    
     		<div class="row">
     			<div class="col-md-7 form_add_pro">
    			    <h3 class="text-center pt-2">Edite produit</h3>
    
     				<form action="update_produit.php?id=<?php echo $get_id;?>" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white">
     					<div class="form-group">
     						<label>Nom:</label>
     						<input type="text" name="nom" class="form-control" placeholder="nom" value='<?php echo $rs['nom']; ?>'>
     					</div>
    					 <div class="form-group">
     						<label>Description:</label>
     						<textarea name="description" class="form-control" placeholder="description" rows="3"><?php echo $rs['description']; ?></textarea>
     					</div>
     					<div class="form-group">
     						<label>Prix1:</label>
     						<input type="number" name="prix1" class="form-control" placeholder="prix" value='<?php echo $rs['prix1']; ?>'>
     					</div>
						<div class="form-group">
     						<label>categorie_id:</label>
							<select class="form-select" name="categorie_id">
							   <?php
                                    $get_all_categories = "SELECT * FROM categories";
                                    $res_cat = mysqli_query($conn, $get_all_categories);

                                    if (mysqli_num_rows($res_cat) > 0) {
                                      while ($rs_cat = mysqli_fetch_assoc($res_cat)) {
                                        ?>
                                        <option value="<?php echo $rs_cat['id']; ?>" <?php echo $rs_cat['id'] ==  $rs['categorie_id'] ? 'selected':'' ?>><?php echo $rs_cat['nom']; ?></option>
                                        <?php
                                      }
									}
                                ?>
							</select>
     					</div>

						 <div class="form-group">
     						<label>Stock:</label>
     						<input type="number" name="stock" class="form-control" placeholder="stock" value='<?php echo $rs['stock']; ?>'>
     					</div>

						 <div class="form-group mt-3">
                            <label>Image</label>
                            <input type="file" class="form-control-file" name="image">
                        </div>

     					<input type="submit" name="submit" class="btn btn-warning mt-3">
     				</form>
     			</div>
     		</div>
    
     </main>

<?php
	  }
	}
?>


<?php include("../../../layouts/vendeur/footer.php") ?>