<?php
include_once('../../connection_db.php');
include("../../layouts/header.php");

// Initialize the session
//session_start();
 
// Check if the user is logged in, if not then redirect him to login page
// if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
//     header("location: ../../auth/inscri-conn.php");
//     exit;
// }

   $client_id = $_SESSION["client_id"]??null;
   $get_id = $_REQUEST['id'];
   $get_item = "SELECT * FROM produits WHERE id = ".$get_id;
   $res = mysqli_query($conn,$get_item);
   if(mysqli_num_rows($res)>0){
	 while($rs = mysqli_fetch_assoc($res)){
?>


<!-- Main -->
<main class="container" style="margin-top: 100px;">
    <h3 class="mb-3"><?php echo $rs['nom']; ?></h3>
    <div class="row mt-5 mb-5">
        <div class="col-md-4">
            <img class="img_pro" src="../../assets/images/produits/<?php echo $rs['image']; ?>" alt="...">
        </div>
        <div class="col-md-8">
            <p>
                <?php echo $rs['description']; ?>
            </p>
            <h4>Prix: <?php echo $rs['prix1']??"_"; ?>Dh</h4>
            <a class="btn btn-sm btn-warning mt-2" href="../panier/panier.php?action=ajout&amp;id=<?php echo $get_id; ?>&amp;l=<?php echo $rs['nom']; ?>&amp;q=1&amp;p=<?php echo $rs['prix2']; ?>">
                <i class="fa-solid fa-plus"></i> Ajouter au panier
            </a>
        </div>
    </div>

    <div class="row mt-5 mb-5">
        <h5 class="mb-3" style="color: chocolate !important;">Commentaires</h5>
        <ul class="list-group list-group-flush mb-5">
        <?php
        $get_all_produits = "SELECT c.*, cl.nom_complet FROM commentaires c, clients cl WHERE cl.id = c.client_id AND c.produit_id = $get_id";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
                <li class="list-group-item p-3 shadow bg-white rounded">
                    <?php  
                        if($rs['type'] == "image") echo "<img src=\"../../assets/images/commentaires/".$rs['valeur']."\" width = \"100\" alt='...'>";
                        else echo "<p>".$rs['valeur']."</p>"; 
                    ?>
                    <strong class="text-muted small"><?php echo $rs['nom_complet']."  ". $rs['date_creation']; ?></strong>
                </li>
        <?php
          }
        } else {
          ?>
            <p class="text-center">Pas de données ...</p>
        <?php
        }
        ?>
        </ul>


        <?php if($client_id){ ?>
        <form action="add_comment_produit.php" method="post" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white rounded">
            <input type="hidden" name="produit_id" value="<?php echo $get_id; ?>">
            <div class="mb-3">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="type" value="texte" onclick="show_text();" checked>
                    <label class="form-check-label" for="inlineCheckbox1">Text</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="type" value="image" onclick="show_image();">
                  <label class="form-check-label" for="inlineCheckbox2">Image</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="type" value="etoile" onclick="show_etoile();">
                  <label class="form-check-label" for="inlineCheckbox3">Etoile</label>
                </div>
            </div>
            <div id="commentaire_text">
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="3" name="texte"></textarea>
                </div>
            </div>
            <div id="commentaire_img" style="display: none;">
               <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control-file" name="image">
                </div>
            </div>
            <div id="commentaire_etoile" style="display: none;">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="1 étoile" name="etoile">
                    <label class="form-check-label">
                        <i class="fa-solid fa-star"></i>
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="2 étoiles" name="etoile">
                    <label class="form-check-label">
                        <i class="fa-solid fa-star"></i> <i class="fa-solid fa-star"></i>
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="3 étoiles" name="etoile">
                    <label class="form-check-label">
                         <i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i>
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="4 étoiles" name="etoile">
                    <label class="form-check-label">
                         <i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i>
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="5 étoiles" name="etoile">
                    <label class="form-check-label">
                         <i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i><i class="fa-solid fa-star"></i>
                    </label>
                </div>
            </div>

            <button type="submit" class="btn btn-warning mt-3">Commenter</button>
        </form>
        
        <?php } ?>
</main>
<!-- End Main -->
<?php
	  }
	}
?>

<?php include("../../layouts/footer.php") ?>