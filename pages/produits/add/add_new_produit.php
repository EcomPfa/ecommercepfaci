<?php include("../../../layouts/vendeur/header.php") ?>
<?php include_once('../../../connection_db.php'); ?>

    <main class="main-container">
    
     	<!-- <div class="container"> -->
     		<div class="row">
     			<div class="col-md-7 form_add_pro">
    			    <h3 class="text-center pt-2">Nouveau produit</h3>
    
     				<form action="insert_produit.php" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white">
     					<div class="form-group">
     						<label>Nom:</label>
     						<input type="text" name="nom" class="form-control" placeholder="nom">
     					</div>
    					 <div class="form-group">
     						<label>description:</label>
     						<textarea name="description" class="form-control" placeholder="description" rows="3"></textarea>
     					</div>
     					<div class="form-group">
     						<label>Prix:</label>
     						<input type="number" name="prix1" class="form-control" placeholder="prix1">
     					</div>
						 <div class="form-group">
     						<label>Categorie:</label>
							<select class="form-select" name="categorie_id">
							   <option value="" disabled selected>veuillez choisie ...</option>
							   <?php
                                    $get_all_categories = "SELECT * FROM categories";
                                    $res_cat = mysqli_query($conn, $get_all_categories);

                                    if (mysqli_num_rows($res_cat) > 0) {
                                      while ($rs_cat = mysqli_fetch_assoc($res_cat)) {
                                        ?>
                                        <option value="<?php echo $rs_cat['id']; ?>"><?php echo $rs_cat['nom']; ?></option>
                                        <?php
                                      }
									}
                                ?>
							</select>
     					</div>

						 <div class="form-group">
     						<label>Stock:</label>
     						<input type="number" name="stock" class="form-control" placeholder="stock">
     					</div>

						 <div class="form-group mt-3">
                            <label>Image</label>
                            <input type="file" class="form-control-file" name="image">
                        </div>

     					<input type="submit" name="submit" class="btn btn-warning mt-3">
     				</form>
     			</div>
     		</div>
     	<!-- </div> -->
    
    </main>
 
 <?php include("../../../layouts/vendeur/footer.php") ?>