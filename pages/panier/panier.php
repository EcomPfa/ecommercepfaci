<?php
include("../../layouts/header.php");

session_start();
include_once("fonctions-panier.php");

  
 // Check if the user is logged in, if not then redirect him to login page
 if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
     header("location: ../../auth/inscri-conn.php");
     exit;
  }

$erreur = false;

$action = (isset($_POST['action'])? $_POST['action']:  (isset($_GET['action'])? $_GET['action']:null ));
if($action !== null)
{
   if(!in_array($action,array('ajout', 'suppression', 'refresh','enregistrer')))
   $erreur=true;

   //récupération des variables en POST ou GET
   $id = (isset($_POST['id'])? $_POST['id']:  (isset($_GET['id'])? $_GET['id']:null )) ;
   $l = (isset($_POST['l'])? $_POST['l']:  (isset($_GET['l'])? $_GET['l']:null )) ;
   $p = (isset($_POST['p'])? $_POST['p']:  (isset($_GET['p'])? $_GET['p']:null )) ;
   $q = (isset($_POST['q'])? $_POST['q']:  (isset($_GET['q'])? $_GET['q']:null )) ;

   //Suppression des espaces verticaux
   $l = preg_replace('#\v#', '',$l);
   //On vérifie que $p est un float
   $p = floatval($p);

   //On traite $q qui peut être un entier simple ou un tableau d'entiers
    
   if (is_array($q)){
      $QteArticle = array();
      $i=0;
      foreach ($q as $contenu){
         $QteArticle[$i++] = intval($contenu);
      }
   }
   else
   $q = intval($q);
    
}

if (!$erreur){
   switch($action){
      Case "ajout":
         ajouterArticle($id,$l,$q,$p);
         break;

      Case "suppression":
         supprimerArticle($l);
         break;

      Case "refresh" :
         for ($i = 0 ; $i < count($QteArticle) ; $i++)
         {
            modifierQTeArticle($_SESSION['panier']['libelleProduit'][$i],round($QteArticle[$i]));
         }
         break;

       Case "enregistrer":
         enregisterPanier();
         break;

      Default:
         break;
   }
}


?>

<div class="container" style="margin-top: 70px;">
    <div class="row">
    <form method="post" action="panier.php">

        <h3 class="mt-3">Votre panier</h3>
         
         <table class="table table-sm shadow p-3 mb-5 bg-white rounded">
            <thead class="table-active">
                <tr>
                    <td class="px-3">Nom</td>
                    <td width = "120">Quantité</td>
                    <td width = "120">Prix Unitaire</td>
                    <td width = "120">Action</td>
                </tr>
            </thead>
         
             <?php
             if (creationPanier())
             {
                $nbArticles=count($_SESSION['panier']['libelleProduit']);
                if ($nbArticles <= 0){ ?>
                  <tr>
                     <td class="text-center" colspan="4">
                        <p>Votre panier est vide !</p> 
                        <img class="icone_panier" src="../../assets/images/icone_panier.png" alt="...">
                        <p>Ajouter des produits à votre panier</p> 
                     </td>
                  </tr>
                <?php } else
                {
                   for ($i=0 ;$i < $nbArticles ; $i++)
                   {
                      echo "<tr>";
                      echo "<td>".htmlspecialchars($_SESSION['panier']['libelleProduit'][$i])."</ td>";
                      echo "<td><input class=\"form-control\" type=\"number\" size=\"4\" name=\"q[]\" value=\"".htmlspecialchars($_SESSION['panier']['qteProduit'][$i])."\"/></td>";
                      echo "<td>".htmlspecialchars($_SESSION['panier']['prixProduit'][$i])."</td>";
                      echo "<td><a class=\"text-warning\" href=\"".htmlspecialchars("panier.php?action=suppression&l=".rawurlencode($_SESSION['panier']['libelleProduit'][$i]))."\">Supprimer</a></td>";
                      echo "</tr>";
                   }?>
         
                   <tr><td colspan="2"></td>
                   <td colspan="2" class="text-danger">
                       Total : <?php echo MontantGlobal() ?>
                   </td></tr>
         
                   <tr>
                     <td colspan="4">
                        <a class="btn btn-warning btn-sm" href="../produits/client_produits.php">
                           Retour
                        </a>
                        <input class="btn btn-warning btn-sm" type="submit" value="Rafraichir"/>
                        <input type="hidden" name="action" value="refresh"/>
   
                        <a class="btn btn-warning btn-sm" href="panier.php?action=enregistrer"> Confirmer la commande</a>
                     </td>
                  </tr>
                   <?php
                }
             }
             ?>
         </table>
        </form>
    </div>
   </div>

   <?php include("../../layouts/footer.php") ?>