<?php 
    //session_start();
    include_once('../../../connection_db.php');
    include("../../../layouts/header.php");

    $get_id = $_REQUEST['panier_id'];
    $client_id = $_SESSION['client_id'];

    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
      header("location: ../../../auth/inscri-conn.php");
      exit;
    }

?>

<main class="container" style="margin-top: 100px;">
    <h3 class="mb-3"> Produits acheter</h3>
    <div class="row">
        <?php
        $get_all_produits = "SELECT dp.* ,p.* FROM detail_panier dp, produits p, panier pp WHERE dp.produit_id = p.id AND pp.id=dp.panier_id AND pp.id_client = $client_id";

        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
            while ($rs = mysqli_fetch_assoc($res)) {
                ?>

                <div class="col-md-4 mb-4">
                    <div class="card">
                        <img class="card-img-top"
                            src="../../../assets/images/produits/<?php echo $rs['image']; ?>"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo $rs['nom']; ?>
                            </h5>
                            <p class="card-text">
                                <?php echo $rs['description']; ?>
                            </p>
                            <strong> Prix: <?php echo $rs['prix2'] ?? "_"; ?> Dh</strong><br>
                            <a class="btn btn-sm btn-warning mt-2"
                                href="../../produits/detail_produit.php?id=<?php echo $rs['id']; ?>">
                                commenter
                            </a>
                        </div>
                    </div>
                </div>

                <?php
            }
        } else {
            ?>
        <p>Pas de données ...</p>
        <?php
        }
        ?>
    </div>

    </tbody>
    </table>
</main>
    
<?php include("../../../layouts/footer.php") ?>