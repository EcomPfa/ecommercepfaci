<?php

function creationPanier()
{
    if (!isset($_SESSION['panier'])) {
        $_SESSION['panier'] = array();
        $_SESSION['panier']['produit_id'] = array();
        $_SESSION['panier']['libelleProduit'] = array();
        $_SESSION['panier']['qteProduit'] = array();
        $_SESSION['panier']['prixProduit'] = array();
        $_SESSION['panier']['verrou'] = false;
    }
    return true;
}

function ajouterArticle($produit_id, $libelleProduit, $qteProduit, $prixProduit){
    //Si le panier existe
    if (creationPanier() && !isVerrouille()) {
        //Si le produit existe déjà on ajoute seulement la quantité
        $positionProduit = array_search($libelleProduit, $_SESSION['panier']['libelleProduit']);

        if ($positionProduit !== false) {
            $_SESSION['panier']['qteProduit'][$positionProduit] += $qteProduit;
        } else {
            //Sinon on ajoute le produit
            array_push($_SESSION['panier']['produit_id'], $produit_id);
            array_push($_SESSION['panier']['libelleProduit'], $libelleProduit);
            array_push($_SESSION['panier']['qteProduit'], $qteProduit);
            array_push($_SESSION['panier']['prixProduit'], $prixProduit);
        }
    } else
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}

function supprimerArticle($libelleProduit)
{
    //Si le panier existe
    if (creationPanier() && !isVerrouille()) {
        //Nous allons passer par un panier temporaire
        $tmp = array();
        $tmp['produit_id'] = array();
        $tmp['libelleProduit'] = array();
        $tmp['qteProduit'] = array();
        $tmp['prixProduit'] = array();
        $tmp['verrou'] = $_SESSION['panier']['verrou'];

        for ($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++) {
            if ($_SESSION['panier']['libelleProduit'][$i] !== $libelleProduit) {
                array_push($tmp['produit_id'], $_SESSION['panier']['produit_id'][$i]);
                array_push($tmp['libelleProduit'], $_SESSION['panier']['libelleProduit'][$i]);
                array_push($tmp['qteProduit'], $_SESSION['panier']['qteProduit'][$i]);
                array_push($tmp['prixProduit'], $_SESSION['panier']['prixProduit'][$i]);
            }

        }
        //On remplace le panier en session par notre panier temporaire à jour
        $_SESSION['panier'] = $tmp;
        //On efface notre panier temporaire
        unset($tmp);
    } else
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}

function modifierQTeArticle($libelleProduit, $qteProduit)
{
    //Si le panier existe
    if (creationPanier() && !isVerrouille()) {
        //Si la quantité est positive on modifie sinon on supprime l'article
        if ($qteProduit > 0) {
            //Recherche du produit dans le panier
            $positionProduit = array_search($libelleProduit, $_SESSION['panier']['libelleProduit']);

            if ($positionProduit !== false) {
                $_SESSION['panier']['qteProduit'][$positionProduit] = $qteProduit;
            }
        } else
            supprimerArticle($libelleProduit);
    } else
        echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}

function MontantGlobal()
{
    $total = 0;
    for ($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++) {
        $total += $_SESSION['panier']['qteProduit'][$i] * $_SESSION['panier']['prixProduit'][$i];
    }
    return $total;
}

function isVerrouille()
{
    if (isset($_SESSION['panier']) && $_SESSION['panier']['verrou'])
        return true;
    else
        return false;
}

function compterArticles()
{
    if (isset($_SESSION['panier']))
        return count($_SESSION['panier']['libelleProduit']);
    else
        return 0;

}

function supprimePanier()
{
    unset($_SESSION['panier']);
}


function enregisterPanier()
{
    if (creationPanier()){
        include_once('../../connection_db.php');
        //panier
        $client_id = $_SESSION['client_id'];
        //
        $sql = "SELECT nb_points FROM `clients` WHERE id = $client_id";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();
        $nb_points = $result->fetch_assoc()['nb_points'];

        //remise a partir nb points
        if($nb_points > 10) $prix_total = MontantGlobal() - ((MontantGlobal()*5)/100);
        else $prix_total = MontantGlobal();

    
        $insert = "INSERT INTO `panier`(`id_client`,`prix_total`) VALUES ($client_id , '$prix_total')";
	    $res = mysqli_query($conn, $insert);
	    if ($res) {
            //detail panier
              $panier_id = $conn->insert_id;

                $nbArticles=count($_SESSION['panier']['libelleProduit']);
                if ($nbArticles > 0){
                    for ($i=0 ;$i < $nbArticles ; $i++){
                        //
                        $produit_id = htmlspecialchars($_SESSION['panier']['produit_id'][$i]);
                        $qte = htmlspecialchars($_SESSION['panier']['qteProduit'][$i]);
                        $prix_u = htmlspecialchars($_SESSION['panier']['prixProduit'][$i]);
                        $total = $qte * $prix_u;
                        //
                        $sql = "SELECT stock FROM `produits` WHERE id = $produit_id";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        $stock = $result->fetch_assoc()['stock'];
                        //
                        if(($stock - $qte) >= 0){
                            //
                            $update = "UPDATE `produits` SET `stock`='($stock - $qte) ' WHERE id = $produit_id";
                            $res = mysqli_query($conn, $update);
                            //
                            $insert = "INSERT INTO `detail_panier`(`panier_id`,`produit_id`,`qte`, `prix_u`, `total`) VALUES ('$panier_id', '$produit_id', '$qte', '$prix_u', '$total')";
                            $res = mysqli_query($conn, $insert);
                        }
                        else {
                            if($nb_points > 10) $new_prix_total = (MontantGlobal() - ($qte*$prix_u)) - (((MontantGlobal() - ($qte*$prix_u))*5)/100);
                            else $new_prix_total = $prix_total-($qte*$prix_u);
                            
                            $update = "UPDATE `panier` SET `prix_total`='$new_prix_total' WHERE id = $panier_id";
                            $res = mysqli_query($conn, $update);
                        }
                        
                    }
         
                }

                supprimePanier();
                header("location: produits/list.php?panier_id=$panier_id");
	    }
	    else{
	       echo "fail";
	    }
    }
}



?>