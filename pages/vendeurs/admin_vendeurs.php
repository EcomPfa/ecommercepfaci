<?php include_once('../../connection_db.php'); ?>

<?php include("../../layouts/admin/header.php") ?>


    <!-- Main -->
    <main class="main-container">
        <h3 class="mb-3">Vendeurs</h3>

        <table class="table shadow-sm p-3 mb-5 bg-white">
      <thead>
        <tr>
          <th>Nom complet</th>
          <th>Photo</th>
          <th>CIN</th>
          <th>Email</th>
          <th>Nom boutique</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $get_all_produits = "SELECT * FROM vendeurs";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
            <tr>
              <td>
                <?php echo $rs['nom_complet']; ?>
              </td>
              <td>
                <?php echo $rs['photo_profil']; ?>
              </td>
              <td>
                <?php echo $rs['CIN']; ?>
              </td>

              <td>
                <?php echo $rs['email']; ?>
              </td>
              <td>
                <?php echo $rs['nom_boutique']; ?>
              </td>
            </tr>
            <?php
          }
        } else {
          ?>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Pas de données ...</p>
          </td>
        </tr>
        <?php
        }
        ?>


      </tbody>
    </table>
    </main>
    <!-- End Main -->

<?php include("../../layouts/admin/footer.php") ?>