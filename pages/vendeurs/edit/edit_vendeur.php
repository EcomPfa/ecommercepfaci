<?php include("../../../layouts/vendeur/header.php") ?>
<?php include_once('../../../connection_db.php'); ?>

<?php

   session_start(); // Démarrer la session
   $get_id = $_SESSION['vendeur_id'];

   $get_item = "SELECT * FROM vendeurs WHERE id = ".$get_id;
   $res = mysqli_query($conn,$get_item);
   if(mysqli_num_rows($res)>0){
	 while($rs = mysqli_fetch_assoc($res)){
	   ?>

        <main class="main-container">
    
     		<div class="row">
     			<div class="col-md-7 form_add_pro" style="display: block; margin: auto;">
    			    <h3 class="text-center pt-2">Mon profile</h3>
    
     				<form action="update_vendeur.php?id=<?php echo $get_id;?>" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white">
     					<div class="form-group">
     						<label>Nom complet:</label>
     						<input type="text" name="nom_complet" class="form-control" placeholder="nom_complet" value='<?php echo $rs['nom_complet']; ?>'>
     					</div>
     					<div class="form-group">
     						<label>CIN:</label>
     						<input type="text" name="CIN" class="form-control" placeholder="CIN" value='<?php echo $rs['CIN']; ?>'>
     					</div>
						<div class="form-group">
     						<label>E-mail:</label>
     						<input type="email" name="email" class="form-control" placeholder="email" value='<?php echo $rs['email']; ?>'>
     					</div>
                        <div class="form-group">
     						<label>Nom de boutique:</label>
     						<input type="text" name="nom_boutique" class="form-control" placeholder="nom_boutique" value='<?php echo $rs['nom_boutique']; ?>'>
     					</div>
						 <div class="form-group">
     						<label>Mot de passe:</label>
     						<input type="text" name="mdp" class="form-control" placeholder="Mot de passe" value='<?php echo $rs['mdp']; ?>'>
     					</div>

						<div class="form-group mt-3">
                            <label>Image</label>
                            <input type="file" class="form-control-file" name="image">
                        </div>

     					<input type="submit" name="submit" class="btn btn-warning mt-3" value="Enregistrer">
     				</form>
     			</div>
     		</div>
    
    </main>

<?php
	  }
	}
?>


<?php include("../../../layouts/vendeur/footer.php") ?>