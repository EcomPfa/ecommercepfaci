<?php include_once('../../connection_db.php'); ?>
<?php include("../../layouts/admin/header.php") ?>

<!-- Main -->
<main class="main-container">
  <h3 class="mb-3">Les commendes</h3>

  <table class="table shadow-sm p-3 mb-5 bg-white">
    <thead>
      <tr>
        <th scope="col">Non Client</th>
        <th scope="col">Tel Client</th>
        <th scope="col">Email Client</th>
        <th scope="col">Adresse Client</th>
        <th scope="col">Vendeur</th>
        <th scope="col">Libelle</th>
        <th scope="col">Prix</th>
        <th scope="col">Qte</th>
        <th scope="col">Total</th>
        <th scope="col">Date</th>
        <th scope="col">Etat</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $sql = "SELECT cl.nom_complet, cl.telephone, cl.email, cl.adresse, pro.nom as libelle, dp.prix_u, dp.qte, dp.total,v.nom_complet as vendeur, dp.is_confirmed, p.date_creation 
      FROM detail_panier dp, produits pro, clients cl, panier p, vendeurs v
      WHERE dp.produit_id = pro.id AND p.id_client = cl.id AND dp.panier_id = p.id AND v.id = pro.id_vendeur";
      $res = mysqli_query($conn, $sql);
      if (mysqli_num_rows($res) > 0) {
        while ($rs = mysqli_fetch_assoc($res)) {
          ?>
          <tr>
            <td>
              <?php echo $rs['nom_complet']; ?>
            </td>
            <td>
              <?php echo $rs['telephone']; ?>
            </td>
            <td>
              <?php echo $rs['email']; ?>
            </td>
            <td>
              <?php echo $rs['adresse']; ?>
            </td>
            <td>
              <?php echo $rs['vendeur']; ?>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['libelle']; ?>
              </strong>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['prix_u']; ?>
              </strong>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['qte']; ?>
              </strong>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['total']; ?>
              </strong>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['date_creation']; ?>
              </strong>
            </td>
            <td>
              <?php echo $rs['is_confirmed'] == true ? "Confermé":"Non confermé"; ?>
            </td>
          </tr>
          <?php
        }
      } else {
        ?>
      <tr>
        <td colspan="11" style="text-align: center;">
          <p>Pas de données ...</p>
        </td>
      </tr>
      <?php
      }
      ?>


    </tbody>
  </table>
</main>
<!-- End Main -->

<?php include("../../layouts/admin/footer.php") ?>