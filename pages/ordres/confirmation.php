<?php
include_once('../../connection_db.php');

session_start();
$vendeur_id = $_SESSION['vendeur_id'];
?>
<?php include("../../layouts/vendeur/header.php") ?>

<!-- Main -->
<main class="main-container">
  <h3 class="mb-3">Confirmation des commendes</h3>

  <table class="table shadow-sm p-3 mb-5 bg-white">
    <thead>
      <tr>
        <th scope="col">Non Client</th>
        <th scope="col">Telephone</th>
        <th scope="col">Email</th>
        <th scope="col">Adresse</th>
        <th scope="col">Libelle</th>
        <th scope="col">Prix</th>
        <th scope="col">Qte</th>
        <th scope="col">Total</th>
        <th scope="col">Etat commande</th>
      </tr>
    </thead>
    <tbody>
      <?php
      // session_start();
      

      $sql = "SELECT dp.id, cl.nom_complet, cl.telephone, cl.email, cl.adresse, pro.nom as libelle, dp.prix_u, dp.qte, dp.total, dp.is_confirmed 
      FROM detail_panier dp, produits pro, clients cl, panier p
      WHERE dp.produit_id = pro.id AND p.id_client = cl.id AND dp.panier_id = p.id  AND pro.id_vendeur = $vendeur_id ";
      $res = mysqli_query($conn, $sql);
      if (mysqli_num_rows($res) > 0) {
        while ($rs = mysqli_fetch_assoc($res)) {
          ?>
          <tr>
            <td>
              <?php echo $rs['nom_complet']; ?>
            </td>
            <td>
              <?php echo $rs['telephone']; ?>
            </td>
            <td>
              <?php echo $rs['email']; ?>
            </td>
            <td>
              <?php echo $rs['adresse']; ?>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['libelle']; ?>
              </strong>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['prix_u']; ?>
              </strong>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['qte']; ?>
              </strong>
            </td>
            <td>
              <strong class="text-secondary">
                <?php echo $rs['total']; ?>
              </strong>
            </td>
            <td>
              <form action="update_status.php" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="dp_id" value="<?php echo $rs['id']; ?>">
                
                <div class="input-group mb-3">
                  <select class="form-select" aria-describedby="basic-addon2" name="is_confirmed">
                    <option value="1" <?php echo($rs['is_confirmed'] == 1 ? "selected": "") ?>>Confirmé</option>
                    <option value="0" <?php echo($rs['is_confirmed'] == 0 ? "selected": "") ?>>N'est pas confirmé</option>
                  </select>
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit" name="submit">OK</button>
                  </div>
                </div>
              </form>
            </td>
          </tr>
          <?php
        }
      } else {
        ?>
      <tr>
        <td colspan="9" style="text-align: center;">
          <p>Pas de données ...</p>
        </td>
      </tr>
      <?php
      }
      ?>


    </tbody>
  </table>
</main>
<!-- End Main -->

<?php include("../../layouts/vendeur/footer.php") ?>