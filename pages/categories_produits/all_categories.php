<?php include_once('../../connection_db.php'); ?>

<?php 
  session_start();
  $vendeur_id = $_SESSION['vendeur_id'];
  
   if(isset($_SESSION["vendeur_id"])) include("../../layouts/vendeur/header.php");
   else include("../../layouts/admin/header.php");
?>


    <!-- Main -->
    <main class="main-container">
        <h3 class="mb-3">Categories de produit</h3>
        <a href="add/add_new_categorie.php" class="btn btn-warning mb-2 pull-right mb-3">Nouveau categorie</a>

        <table class="table shadow-sm p-3 mb-5 bg-white">
      <thead>
        <tr>
          <th>Nom</th>
          <th>Description</th>
          <th width="130">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $get_all_produits = "SELECT * FROM categories";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
            <tr>
              <td>
                <?php echo $rs['nom']; ?>
              </td>
              <td>
                <?php echo $rs['description']; ?>
              </td>
              <!-- <td>
                <?php echo $rs['prix2']; ?>
              </td> -->
              
              <td>
                <form action="delete_categorie.php" method="post">
                  <a href="edit/edit_categorie.php?id=<?php echo $rs['id']; ?>" class="btn btn-warning me-2"> 
                    <i class="fa-solid fa-pen"></i>
                  </a>

                  <input type="hidden" name="id" value="<?php echo $rs['id']; ?>">
                  <button type="submit" name="submit" class="btn btn-danger">
                    <i class="fa-solid fa-trash"></i>
                  </button>
                </form>
                
            </td>
            </tr>
            <?php
          }
        } else {
          ?>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Pas de données ...</p>
          </td>
        </tr>
        <?php
        }
        ?>


      </tbody>
    </table>
    </main>
    <!-- End Main -->

<?php 
  if(isset($_SESSION["vendeur_id"])) include("../../layouts/vendeur/footer.php");
  else include("../../layouts/admin/footer.php");
?>