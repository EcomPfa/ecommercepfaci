<?php
   include_once('../../connection_db.php');
?>

<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
// if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
//     header("location: ../../auth/inscri-conn.php");
//     exit;
// }
?>


<?php include("../../layouts/header.php") ?>

<!-- Main -->
<main class="container" style="margin-top: 60px;">
    <h3 class="mb-3">les categories</h3>
    <div class="row">
        <?php
        $get_all_produits = "SELECT * FROM categories";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
            while ($rs = mysqli_fetch_assoc($res)) {
                ?>

                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo $rs['nom']; ?>
                            </h5>
                            <p class="card-text">
                                <?php echo $rs['description']; ?>
                            </p>
                            <a class="btn btn-sm btn-warning mt-2"
                                href="../produits/produits_by_categorie.php?id=<?php echo $rs['id']; ?>&nom_cat=<?php echo $rs['nom']; ?>">
                                Voir produits
                            </a>
                        </div>
                    </div>
                </div>

        <?php
            }
            } else {
            ?>
        <p>Pas de données ...</p>
        <?php
        }
        ?>
    </div>





    </tbody>
    </table>
</main>
<!-- End Main -->

<?php include("../../layouts/footer.php") ?>