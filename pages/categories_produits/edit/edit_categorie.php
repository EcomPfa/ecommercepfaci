<?php include_once('../../../connection_db.php'); ?>

<?php 
  session_start();
  $vendeur_id = $_SESSION['vendeur_id'];
  
   if(isset($_SESSION["vendeur_id"])) include("../../../layouts/vendeur/header.php");
   else include("../../../layouts/admin/header.php");
?>

<?php
   $get_id = $_REQUEST['id'];
   $get_item = "SELECT * FROM categories WHERE id = ".$get_id;
   $res = mysqli_query($conn,$get_item);
   if(mysqli_num_rows($res)>0){
	 while($rs = mysqli_fetch_assoc($res)){
	   ?>

<main class="main-container">
    
     		<div class="row">
     			<div class="col-md-7 form_add_pro">
    			    <h3 class="text-center pt-2">Edit categorie</h3>
    
     				<form action="update_categorie.php?id=<?php echo $get_id;?>" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white">
     					<div class="form-group">
     						<label>Nom:</label>
     						<input type="text" name="nom" class="form-control" placeholder="nom" value='<?php echo $rs['nom']; ?>'>
     					</div>
    					 <div class="form-group">
     						<label>description:</label>
     						<textarea name="description" class="form-control" placeholder="description" rows="3"><?php echo $rs['description']; ?></textarea>
     					</div>

     					<input type="submit" name="submit" class="btn btn-warning mt-3">
     				</form>
     			</div>
     		</div>
    
    </main>

<?php
	  }
	}
?>


<?php 
  if(isset($_SESSION["vendeur_id"])) include("../../../layouts/vendeur/footer.php");
  else include("../../../layouts/admin/footer.php");
?>