<?php include_once('../../../connection_db.php'); ?>

<?php 
  session_start();
  $vendeur_id = $_SESSION['vendeur_id'];
  
   if(isset($_SESSION["vendeur_id"])) include("../../../layouts/vendeur/header.php");
   else include("../../../layouts/admin/header.php");
?>

    <main class="main-container">
    
     	<!-- <div class="container"> -->
     		<div class="row">
     			<div class="col-md-7 form_add_pro">
    			    <h3 class="text-center pt-2">Nouveau categorie</h3>
    
     				<form action="insert_categorie.php" method="POST" enctype="multipart/form-data" class="shadow p-3 mb-5 bg-white">
     					<div class="form-group">
     						<label>Nom:</label>
     						<input type="text" name="nom" class="form-control" placeholder="nom">
     					</div>
    					 <div class="form-group">
     						<label>description:</label>
     						<textarea name="description" class="form-control" placeholder="description" rows="3"></textarea>
     					</div>

     					<input type="submit" name="submit" class="btn btn-warning mt-3">
     				</form>
     			</div>
     		</div>
     	<!-- </div> -->
    
    </main>
 
<?php 
  if(isset($_SESSION["vendeur_id"])) include("../../../layouts/vendeur/footer.php");
  else include("../../../layouts/admin/footer.php");
?>