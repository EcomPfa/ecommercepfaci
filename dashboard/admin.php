<?php

include_once('../connection_db.php');

$sql = "SELECT COUNT(*) as nb_produits FROM `produits`";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$nb_products = $result->fetch_assoc()['nb_produits'];

$sql = "SELECT COUNT(*) as nb_clients FROM `clients`";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$nb_clients = $result->fetch_assoc()['nb_clients'];

$sql = "SELECT COUNT(*) as nb_clients FROM `clients` WHERE compte_active = 1;";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$comptes_actives = $result->fetch_assoc()['nb_clients'];

$sql = "SELECT COUNT(*) as nb_vendeurs FROM `vendeurs`";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$nb_vendeurs = $result->fetch_assoc()['nb_vendeurs'];

?>

<?php include("../layouts/admin/header.php") ?>

      <!-- Main -->
      <main class="main-container">
        <div class="main-title">
          <p class="font-weight-bold">DASHBOARD</p>
        </div>

        <div class="main-cards">

          <div class="card">
            <div class="card-inner">
              <p class="text-primary">PRODUITS</p>
              <span class="material-icons-outlined text-blue">inventory_2</span>
            </div>
            <span class="text-primary font-weight-bold"><?php echo $nb_products; ?></span>
          </div>  

          <div class="card">
            <div class="card-inner">
              <p class="text-warning">CLIENTS</p>
              <span class="material-icons-outlined text-orange">
                groups_2
                </span>
            </div>
            <span class="text-warning font-weight-bold"> <?php echo $nb_clients; ?></span>
          </div>

          <div class="card">
            <div class="card-inner">
                <p class="text-success">COMPTES ACTIVES</p>
                <span class="material-icons-outlined text-green">
                  person
                </span>
            </div>
            <span class="text-success font-weight-bold"><?php echo $comptes_actives; ?></span>
          </div>

          <div class="card">
           
            <div class="card-inner">
              <p class="text-danger">VENDEURS</p>
              <span class="material-icons-outlined text-red">messages</span>
            </div>
            <span class="text-danger font-weight-bold"> <?php echo $nb_vendeurs; ?> </span>
          </div>

        </div>

        <div class="col-md-12 shadow p-3 mb-5 bg-white rounded">
            <p class="chart-title">Les 5 produits plus vendu</p>
            <table class="table shadow-sm p-3 mb-5 bg-white">
      <thead>
        <tr>
          <th>Nom</th>
          <th>Prix</th>
          <th>vendeur</th>
          <th>quantité vendu</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $get_all_produits = "SELECT produit_id,p.nom, p.prix1, p.stock, SUM(qte) as qte,v.nom_complet as vendeur FROM detail_panier dp, produits p, vendeurs v WHERE p.id = dp.produit_id AND v.id = p.id_vendeur GROUP BY produit_id ORDER BY qte DESC LIMIT 5;";
        $res = mysqli_query($conn, $get_all_produits);
        if (mysqli_num_rows($res) > 0) {
          while ($rs = mysqli_fetch_assoc($res)) {
            ?>
            <tr>
              <td>
                <?php echo $rs['nom']; ?>
              </td>
              <td>
                <?php echo $rs['prix1']; ?>
              </td>
              <td>
                <?php echo $rs['vendeur']; ?>
              </td>
              <td>
                <?php echo $rs['qte']; ?>
              </td>
            </tr>
            <?php
          }
        } else {
          ?>
        <tr>
          <td colspan="5" style="text-align: center;">
            <p>Pas de données ...</p>
          </td>
        </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
        </div>


        </div>
      </main>
      <!-- End Main -->

      <?php include("../layouts/admin/footer.php") ?>