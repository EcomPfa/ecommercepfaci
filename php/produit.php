<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produit Details Page</title>
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link data-minify="1" rel="stylesheet" href="https://themes/boilerplate/style.css?ver=1684769176">
    <link data-minify="1" rel='stylesheet' href='https://www.marlette.fr/wp-content/cache/min/1/wp-content/themes/boilerplate/css/main.css?ver=1684769176'  />
  
    <!-- Rest of your stylesheets -->
  
</head>
<body>
     
  <div class="card">
    <div class="img">
        <!-- Slide images -->
        <?php
        // Fetch slide images from database
        // Replace database credentials with your own
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "ecommerce";
  
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
  
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
  
        // Fetch slide images from database
        $sql = "SELECT image FROM produits";
        $result = $conn->query($sql);
  
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo '<img src="' . $row["image"] . '" alt="" onclick="changeImage(this)"> <br>';
            }
        }
        $conn->close();
        ?>
    </div>

    <div class="flex-box">
        <div class="left">
            <div class="big-img">
                <img src="images/argan2.png">
            </div>
            <div class="images">
                <!-- Small images -->
                <?php
                // Fetch small images from database
                // Replace database credentials with your own
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "ecommerce";
  
                // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
  
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
  
                // Fetch small images from database
                $sql = "SELECT image FROM produits";
                $result = $conn->query($sql);
  
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo '<img src="' . $row["image"] . '" alt="" onclick="changeImage(this)">';
                    }
                }
                $conn->close();
                ?>
            </div>
        </div>
        <div class="right">
            <!-- Product details -->
            <?php
            // Fetch product details from database
            // Replace database credentials with your own
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "ecommerce";
  
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
  
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
  
            // Fetch product details from database
            $sql = "SELECT p.id, p.nom, p.description, p.prix, c.commentaire FROM produits p
                    LEFT JOIN commentaires c ON p.id = c.produit_id
                    WHERE p.id = " . $_GET['id'];
  
            $result = $conn->query($sql);
  
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                echo '<h1>' . $row['nom'] . '</h1>';
                echo '<p>' . $row['description'] . '</p>';
                echo '<h3>Prix: $' . $row['prix'] . '</h3>';
  
                // Display comments if available
                if (!empty($row['commentaire'])) {
                    echo '<h3>Commentaires:</h3>';
                    echo '<p>' . $row['commentaire'] . '</p>';
                }
            }
  
            $conn->close();
            ?>
        </div>
    </div>
</div>
  
<!-- Rest of your HTML content -->
  
<script>
    function clk(img){
        var src = img.src;
        document.querySelector(".big-img img").src = src;
    }
  
    function changeImage(img){
        var src = img.src;
        document.querySelector(".big-img img").src = src;
    }
</script>
</body>
</html>
