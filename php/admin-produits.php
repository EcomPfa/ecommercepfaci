<?php
// Effectuer la connexion à la base de données
$host = 'localhost';
$dbname = 'ecommerce';
$username = 'root';
$password = '';

try {
  $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", $username, $password);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Vérifier si une recherche a été effectuée
  $query = isset($_GET['query']) ? $_GET['query'] : '';
  $produits = [];

  if (!empty($query)) {
    // Effectuer la recherche dans la table "produits" par nom
    $query = $query . '%';
    $searchQuery = "SELECT * FROM produits WHERE nom LIKE :query";
    $statement = $db->prepare($searchQuery);
    $statement->bindParam(':query', $query);
    $statement->execute();
    $produits = $statement->fetchAll(PDO::FETCH_ASSOC);
  } else {
    // Récupérer tous les produits depuis la table "produits"
    $selectAllQuery = "SELECT * FROM produits";
    $statement = $db->prepare($selectAllQuery);
    $statement->execute();
    $produits = $statement->fetchAll(PDO::FETCH_ASSOC);
  }

  // Afficher les produits dans un tableau
  echo '
  <html lang="fr">
    <head>
      <meta charset="UTF-8" />
      <style>
        table {
          border-collapse: collapse;
          width: 100%;
        }

        th, td {
          padding: 8px;
          text-align: left;
          border-bottom: 1px solid #ddd;
        }

        th {
          background-color: chocolate;
          color: white;
        }

        h1 {
          color: chocolate;
          text-align: center;
        }
        button[type="submit"] {
            padding: 10px 20px;
            background-color: chocolate;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
          }
          
          button[type="submit"]:hover {
            background-color: chocolate;
          }
          
          button[type="submit"]:focus {
            outline: none;
          }
          
          button[type="submit"]:active {
            background-color:chocolate;
          }
          .search-bar {
            width: max-content;
          }
          input[type="text"][name="query"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px;
          }
          
          input[type="text"][name="query"]:focus {
            outline: none;
            border-color: chocolate;
          }
      </style>
      <script>
        document.addEventListener("DOMContentLoaded", function() {
          var actionIcons = document.querySelectorAll(".action-icon");
          actionIcons.forEach(function(icon) {
            icon.addEventListener("click", function() {
              var description = this.nextElementSibling;
              if (description.style.display === "none") {
                description.style.display = "inline";
              } else {
                description.style.display = "none";
              }
            });
          });
        });
      </script>
    </head>
    <body>
    ';
    include("sidebar-header.php");
    echo'
      <br>
      <h1> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      &nbsp;Liste des produits</h1>
      <br> <div class="center-content">
      <!-- Barre de recherche -->
      <div class="search-bar">
        <form action="" method="get">
          <input type="text" name="query" placeholder="Rechercher les produits" value="' . htmlspecialchars($query) . '"> 
          <button type="submit"><i class="fa fa-search">Rechercher</i></button>                               
        </form>
      </div>
      <br><br>';

  if (!empty($produits)) {
    // Afficher les résultats de la recherche ou tous les produits
    echo '
      <table>
        <tr>
          <th>ID</th>
          <th>Nom</th>
          <th>ID Vendeur</th>
          <th>Catégorie</th>
          <th>Prix 1</th>
          <th>Prix 2</th>
          <th>Stock</th>
        </tr>';

    foreach ($produits as $produit) {
      echo '<tr>
              <td>' . $produit['id'] . '</td>
              <td>' . $produit['nom'] . '</td>
              <td>' . $produit['id_vendeur'] . '</td>
              <td>' . $produit['categorie'] . '</td>
              <td>' . $produit['prix1'] . '</td>
              <td>' . $produit['prix2'] . '</td>
              <td>' . $produit['stock'] . '</td>
            </tr>';
    }

    echo '</table>';
  } else {
    // Aucun produit trouvé
    echo '<p>Aucun produit trouvé.</p>';
  }

  echo '</div></body>
  </html>';
} catch (PDOException $e) {
  // Gérer les erreurs de connexion à la base de données
  echo "Erreur de connexion à la base de données : " . $e->getMessage();
}
?>
