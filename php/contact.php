<?php
// Vérifier si le formulaire a été soumis
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // Effectuer la connexion à la base de données
  $host = 'localhost';
  $dbname = 'ecommerce';
  $username = 'root';
  $password = '';

  try {
    $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Récupérer les données du formulaire
    $nom_complet = $_POST['nom_complet'];
    $email = $_POST['email'];
    $telephone = $_POST['telephone'];
    $message = $_POST['message'];

    // Insérer les données dans la table "messages"
    $query = "INSERT INTO messages (nom_complet, email, telephone, message) VALUES (:nom_complet, :email, :telephone, :message)";
    $statement = $db->prepare($query);
    $statement->bindParam(':nomComplet', $nom_complet);
    $statement->bindParam(':email', $email);
    $statement->bindParam(':telephone', $telephone);
    $statement->bindParam(':message', $message);
    $statement->execute();

    // Rediriger l'utilisateur vers une page de confirmation ou afficher un message de succès
    header("Location: confirmation.php");
    exit;
  } catch (PDOException $e) {
    // Gérer les erreurs de connexion à la base de données
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
  }
}
?>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    
   
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="page-acceuil.php">
<style>
@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap");

.f {
  margin: 0 auto; /* Centrer le formulaire horizontalement */
  padding: 30px;
  background-color: white;
  border-radius: 10px;
  border: 1.5px solid #d8af6e;
  width: 500px; /* Largeur du formulaire */
  font-family: "Roboto", sans-serif;
  font-weight: bold;
}

form h1 {
  font-size: 20px;
}
form .separation {
  width: 100%;
  height: 1px;
  background-color: chocolate;
}
form .corps-formulaire {
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 30px;
}
form .corps-formulaire .groupe {
  position: relative; /* Pour mettre positionner l’élément dans le flux normal de la page */
  margin-top: 20px;
  display: flex;
  flex-direction: column;
}
form .corps-formulaire .gauche .groupe input {
  margin-top: 5px;
  padding: 10px 5px 10px 30px;
  border: 1px solid #c9c9c9;
  outline-color: #dfbd74;
  border-radius: 5px;
}
form .corps-formulaire .gauche .groupe i {
  position: absolute; /* positionné par rapport à son parent le plus proche positionné */
  left: 0;
  top: 25px;
  padding: 9px 8px;
  color: #ce9b2c;
}
form .corps-formulaire .droite {
  margin-left: 40px;
}
form .corps-formulaire .droite .groupe {
  height: 100%;
 
}
form .corps-formulaire .droite .groupe textarea {
  margin-top: 5px;
  padding: 10px;
  background-color: #fff;
  border: 1.5px solid #d8af6e;
  outline: none;
  border-radius: 5px;
  resize: none;
  height: 72%;
}
form .pied-formulaire button {
  margin-top: 10px;
  background-color: chocolate;
  color: white;
  font-size: 15px;
  border: none;
  padding: 10px 20px;
  border-radius: 5px;
  outline: none;
  cursor: pointer;
  transition: transform 0.5s;
}
form .pied-formulaire button:hover {
  transform: scale(1.05);
}

@media screen and (max-width: 920px) {
  form .corps-formulaire .droite {
    margin-left: 0px;
  }
}
</style>
  </head>
  <body>

<!-- En-tête -->
<header>
  <!-- Logo -->
  <div class="logo">
    <a href="page-acceuil.php"><img src="images/logo.jpeg" alt="Logo"></a> 

  </div>

  <!-- Barre de recherche -->
  <div class="search-bar">
    <form action="recherche.php" method="get">
      <input type="text" name="query" placeholder="Recherche..."> 
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>

  <!-- Boutons de connexion, inscription et panier -->
  <div class="header-buttons">
    <ul>
      <li><a href="inscri-conn.php"><i class="fa fa-user"></i>Connexion &nbsp;</a></li>
     
      <li><a href="panier.php"><i class="fa fa-shopping-cart"></i> Panier</a></li>
    </ul>
  </div>
</header>
<br><br><br><br>   <br><br><br><br>  <br><br>
    <form class="f">
      <h1>Contactez-nous</h1>
      <div class="separation"></div>
      <div class="corps-formulaire">
        <div class="gauche">
          <div class="groupe">
            <label>Votre nom complet</label>
            <input type="text" name="nom_complet" autocomplete="off" required/>
            <i class="fas fa-user"></i>
          </div>
          <div class="groupe">
            <label>Votre adresse e-mail</label>
            <input type="mail" name="email" autocomplete="off" required/>
            <i class="fas fa-envelope"></i>
          </div>
          <div class="groupe">
            <label>Votre téléphone</label>
            <input type="tel" name="telephone" autocomplete="off" required />
            <i class="fas fa-mobile"></i>
          </div>
        </div>

        <div class="droite">
          <div class="groupe">
            <label>Message</label>
            <textarea name="message" placeholder="Saisissez ici..." required></textarea>
          </div>
        </div>
      </div>

      <div class="pied-formulaire" align="center">
        <button>Envoyer le message</button>
      </div>
    </form>
    <br><br><br><br>   <br><br> 
 
    <!-- Pied de page -->
  
  <footer>
    <div class="container">
      <div>
        <h2>Shop</h2>
        <ul>
          <li><a href="page-acceuil.php">Store</a></li>
          <li><a href="#">Gift</a></li>
          <li><a href="#">Student Discount</a></li>
        </ul>
      </div>
      <div>
      </div>
      <div>
        <h2>Quick Links</h2>
        <ul>
          <li><a href="contact.php">Contact us</a></li>
          <li><a href="termesetconditions.php">Terms & Condition</a></li>
        </ul>
      </div>
      <div>
        <h2>Newsletter</h2>
        <div>
          <input type="text" placeholder="Enter your text" />
          <button>Search</button>
        </div>
        <div>
          <ul>
            <li><span class="bi bi-telegram"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-instagram"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-linkedin"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-facebook"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-twitter"></span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          
        </div>
      </div>
    </div>
  </footer>
</body>
</html>