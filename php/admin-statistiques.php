<!DOCTYPE html>
<html>
<head>
  <title>Admin Statistiques</title>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  
  <style>
    h1 {
      text-align: center;
      color: chocolate;
    }
  </style>
</head>
<body>
  <?php include("sidebar-header.php"); ?>
  <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Statistiques des commandes et des utilisateurs</h1>
  <div class="center-content">
  <canvas id="commandesChart"></canvas>
  <canvas id="utilisateursChart"></canvas>

  <script>
    $(document).ready(function() {
      $.ajax({
        url: 'backend/get_commandes_data.php',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
          afficherDiagrammeCommandes(response);
        },
        error: function(xhr, status, error) {
          console.log("Erreur lors de la récupération des données des commandes.");
        }
      });

      $.ajax({
        url: 'backend/get_utilisateurs_data.php',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
          afficherDiagrammeUtilisateurs(response);
        },
        error: function(xhr, status, error) {
          console.log("Erreur lors de la récupération des données des utilisateurs.");
        }
      });
    });

    function afficherDiagrammeCommandes(data) {
      var commandesData = [];

      for (var i = 0; i < data.length; i++) {
        var semaine = new Date(data[i].semaine);
        var commandes = data[i].commandes;
        var commandes_confirmees = data[i].commandes_confirmees;

        commandesData.push({
          x: semaine,
          y: commandes,
          c: commandes_confirmees
        });
      }

      var ctx = document.getElementById('commandesChart').getContext('2d');
      var chart = new Chart(ctx, {
        type: 'line',
        data: {
          datasets: [{
            label: 'Commandes par semaine',
            data: commandesData,
            backgroundColor: 'rgba(0, 123, 255, 0.3)',
            borderColor: 'rgba(0, 123, 255, 1)',
            borderWidth: 2,
            pointRadius: 4,
            pointHoverRadius: 6
          }]
        },
        options: {
          responsive: true,
          scales: {
            x: {
              type: 'time',
              time: {
                unit: 'week',
                displayFormats: {
                  week: 'w'
                }
              },
              title: {
                display: true,
                text: 'Semaines'
              }
            },
            y: {
              title: {
                display: true,
                text: 'Commandes'
              }
            }
          }
        }
      });
    }

    function afficherDiagrammeUtilisateurs(data) {
      var utilisateursData = [];

      for (var i = 0; i < data.length; i++) {
        var semaine = new Date(data[i].semaine);
        var utilisateurs = data[i].utilisateurs;

        utilisateursData.push({
          x: semaine,
          y: utilisateurs
        });
      }

      var ctx = document.getElementById('utilisateursChart').getContext('2d');
      var chart = new Chart(ctx, {
        type: 'line',
        data: {
          datasets: [{
            label: 'Utilisateurs inscrits par semaine',
            data: utilisateursData,
            backgroundColor: 'rgba(255, 99, 132, 0.3)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 2,
            pointRadius: 4,
            pointHoverRadius: 6
          }]
        },
        options: {
          responsive: true,
          scales: {
            x: {
              type: 'time',
              time: {
                unit: 'week',
                displayFormats: {
                  week: 'w'
                }
              },
              title: {
                display: true,
                text: 'Semaines'
              }
            },
            y: {
              title: {
                display: true,
                text: 'Utilisateurs inscrits'
              }
            }
          }
        }
      });
    }
  </script>
  </div>
</body>
</html>
