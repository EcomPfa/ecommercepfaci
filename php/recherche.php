<?php
// Récupérer la valeur de recherche
$query = $_GET['nom_complet'];

// Effectuer les opérations de recherche appropriées avec la valeur $query
// Ici, nous supposons que vous utilisez une base de données pour stocker les produits
// Vous devez vous connecter à la base de données et effectuer une requête appropriée pour rechercher le produit

// Exemple de connexion à la base de données MySQL
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ecommerce";

// Créer une connexion
$conn = new mysqli($servername, $username, $password, $dbname);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion à la base de données a échoué: " . $conn->connect_error);
}

// Préparer la requête de recherche en utilisant une instruction préparée pour éviter les injections SQL
$stmt = $conn->prepare("SELECT * FROM produits WHERE nom_complet = ?");
$stmt->bind_param("s", $query);
$stmt->execute();

// Récupérer les résultats de la recherche
$result = $stmt->get_result();

// Vérifier si le produit existe
if ($result->num_rows > 0) {
    // Produit trouvé, afficher les détails du produit
    while ($row = $result->fetch_assoc()) {
        echo "Produit trouvé : " . $row['nom_complet'];
        // Afficher d'autres détails du produit si nécessaire
    }
} else {
    // Aucun produit trouvé
    echo "Produit inexistant";
}

// Fermer la connexion à la base de données
$stmt->close();
$conn->close();
?>

