<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Ma plate-forme de commerce électronique</title>
  <link rel="icon" type="image/x-icon" href="images/logo.jpeg">
  <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css"
    />
  <link rel="stylesheet" media="screen and (max-width:400px)" href="page-acceuil.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
    /* Styles pour l'en-tête */
*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  header {
    position:fixed;
    top: 0;
    left: 0;
    padding: 10px;
    z-index: 999; /* Assurez-vous que le z-index du header est supérieur aux autres éléments pour qu'il s'affiche correctement */
    
    width: 100%;
    background-color: #fff;
    display: flex;
    align-items:center;
    justify-content: space-between;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }
    
    .logo img {
      height: 70px;
      align-items: center;
  
    }
    
    .search-bar {
      display: flex;
      align-items: 20px;
      margin:10px;
    }
    
    .search-bar input[type="text"] {
      padding: 10px;
      border-radius: 20px;
      border: none;
      background-color: #f2f2f2;
      margin-right: 10px;
      font-size: 16px;
      width: 200px;
    }
    
    .search-bar button {
      padding: 10px 20px;
      border-radius: 20px;
      border: none;
      background-color: chocolate;
      color: #fff;
      cursor: pointer;
      font-size: 16px;
    }
    
    .header-buttons ul {
      list-style: none;
      margin: 0;
      padding: 0;
      display: flex;
    }
    
    .header-buttons li {
      margin-left: 5px;
    }
    
    .header-buttons a {
      color: #2c2c2c;
      text-decoration: none;
      font-size: 16px;
    }
    
    .fa {
      margin-right: 10px;
    }
    
    /* Styles pour le contenu principal */
    
  body{
    
    height: 100vh;
    
  }
    main {
      padding: 50px;
      text-align: center;
    }
    .menu ul {
      list-style-type: none;
    }
    
    .menu ul ul {
      list-style-type:none;
    }
    
    .menu ul ul ul {
      list-style-type: none;
    }
    
    .menu ul ul ul ul {
      list-style-type: none;
    }
    
    nav ul {
      list-style: none;
      margin: 0;
      padding: 0;
      display: flex;
    }
    
    nav li {
      margin-right: 20px;
    }
    
    nav a {
      color: #333;
      text-decoration: none;
      font-size: 18px;
    }
    
    .menu {
      width: 150px; /* set a fixed width */
      background-color: #d8af6e;
      margin: 20px auto;
      text-align: center;
      border-radius: 10px;
    }
    
    .menu > ul {
      padding: 0;
      margin: 0;
    }
    
    .menu > ul > li {
      display: block;
      position: relative;
      margin: 0;
    }
    
    .menu > ul > li > a {
      display: block;
      text-decoration: none;
      color: #fff;
      font-size: 24px;
      padding: 20px 0;
    }
    
    .menu > ul > li > .sub-menu {
      width: 200px;
      position: absolute;
      top: 0;
      left: 100%; /* position the submenu to the right of the parent menu item */
      background-color: #B69361;
      border-radius: 10px;
      opacity: 0;
      visibility: hidden;
      transition: all 0.4s;
    }
    
    .menu > ul > li > .sub-menu > li {
      margin: 6px 0;
      position: relative;
      text-align: center;
    }
    
    .menu > ul > li > .sub-menu > li > a {
      display: block;
      font-size: 21px;
      color: #fff;
      text-decoration: none;
      padding: 10px 0;
      border-bottom: 2px solid #fff;
    }
    
    .menu > ul > li > .sub-menu > li > ul {
      width: 200px;
      position: absolute;
      left: 100%; /* position the sub-submenu to the right of the parent submenu item */
      top: 0;
      background-color: #d7b592;
      border-radius: 10px;
      opacity: 0;
      visibility: hidden;
      transition: all 0.4a;
    }
    
    .menu > ul > li > .sub-menu > li > ul > li {
      margin: 6px 0;
    }
    
    .menu > ul > li > .sub-menu > li > ul > li > a {
      display: block;
      font-size: 21px;
      color: #fff;
      text-decoration: none;
      padding: 10px 0;
      border-bottom: 2px solid #fff;
    }
    
    .menu > ul > li:hover > .sub-menu {
      opacity: 1;
      visibility: visible;
    }
    
    .menu > ul > li > .sub-menu > li:hover > ul {
      opacity: 1;
      visibility: visible;
    }
    
     
     h1, h2, h3, h4, h5, h6 { font-weight: 700; margin-top: 0; margin-bottom: 1rem; }
  
  h1 { font-size: 3rem; }
  
  h2 { font-size: 2.5rem; }
  
  h3 { font-size: 2rem; color:#934d02 }
  
  p { margin-top: 0; margin-bottom: 1rem; }
  
  .container { max-width: 1200px; margin: 0 auto; padding: 0 15px; box-sizing: border-box; }
  .items { display: grid; grid-template-columns: repeat(auto-fit, minmax(250px, 1fr)); grid-gap: 20px; }
  
  .item { text-align: center; }
  
  .item img { max-width: 100%; height: auto; }
  
  .item h3 { font-size: 1.5rem; margin-top: 1rem; margin-bottom: 0.5rem; }
  
  .item p { margin-top: 0; margin-bottom: 1rem; font-size: 1rem; }
  
  .item span { font-weight: 700; font-size: 1.25rem; color:#CC965B ; display: block; margin-bottom: 1rem; }
  
  .item button { background-color: #B69361; color: #fff; border: none; padding: 10px; border-radius: 5px; font-size: 1rem; cursor: pointer; transition: background-color 0.2s ease-in-out; }
  
  .item button:hover { background-color: #B69361; }
        /* Styles pour le pied-page*/
  
      footer{
          width: 100%;
          background-color: #d8af6e;
      }
      .container{
          display: flex;
          align-items: baseline;
          justify-content: space-around;
          width: 90%;
          margin: 20px auto 20px;
      }
      .container>div>h2{
          color: #000000;
          font-family: sans-serif;
          font-size: 24px;
          padding: 20px 0;
          user-select: none;
      }
      .container>div>ul{
          list-style-type: none;
      }
      .container>div>ul>li{
          margin: 16px 0;
          cursor: pointer;
      }
      .container>div>ul>li>a{
          font-size: 20px;
          text-decoration: none;
          font-family: sans-serif;
          position: relative;
          left: 0;
          padding: 2px;
          color: #ececeb;
          font-weight: 700;
      
          transition: all 0.4s;
      }
      .container>div>ul>li>a:hover{
          left: 8px;
      }
      .container>div>div{
          margin: 20px 0 10px;
          padding: 6px 14px;
          border-radius: 7px;
          width: 100%;
          display: flex;
          align-items: center;
          flex-wrap: wrap;
      }
      .container>div>div>input{
          color: #000;
          background-color: #fff;
          padding: 16px 10px;
          border-radius: 4px;
          margin-right: 10px;
          border: none;
          outline: none;
      }
      .container>div>div>button{
          padding: 16px 26px;
          font-size: 14px;
          border: none;
          outline: none;
          border-radius: 4px;
          background-color: #000000;
          color: #fff;
          text-transform: uppercase;
          cursor: pointer;
      }
      .container>div>div>ul{
          list-style-type: none;
      }
      .container>div>div>ul>li{
          display: inline-block;
          margin: 0 4px;
      }
      .container>div>div>ul>li>span{
          background-color: #000000;
          padding: 14px 14px;
          font-size: 20px;
          color: #fff;
          border-radius: 50%;
      }

    </style>
</head>
<body>

  <!-- En-tête -->
  <header>
    <!-- Barre de navigation -->
    <nav>
      <ul> 
        <li><a href="../pagecatgories/categories.php"><i class="fa-solid fa-bars"></i> Categories </a></li>
      </ul>
    </nav>

    <!-- Logo -->
    <div class="logo">
      <a href="page-acceuil.php"><img src="images/logo.jpeg" alt="Logo"></a> 

    </div>

    <!-- Barre de recherche -->
    <div class="search-bar">
      <form action="#" method="get">
        <input type="text" name="query" placeholder="Recherche..."> 
        <button type="submit"><i class="fa fa-search"></i></button>
      </form>
    </div>

    <!-- Boutons de connexion, inscription et panier -->
    <div class="header-buttons">
      <ul>
        <li><a href="inscri-conn.php"><i class="fa fa-user"></i>Connexion &nbsp;</a></li>
       
        <li><a href="panier.php"><i class="fa fa-shopping-cart"></i> Panier</a></li>
      </ul>
    </div>
  </header>
  <br><br><br><br><br>
  <div class="w3-content w3-section" style="max-width:1920px">
    <img class="mySlides" src="images/slide1.jpeg" style="width:100%">
    <img class="mySlides" src="images/slide2.jpeg" style="width:100%">
    <img class="mySlides" src="images/slide3.jpeg" style="width:100%">
  </div>
  <script>
    var myIndex = 0;
    carousel();
    
    function carousel() {
      var i;
      var x = document.getElementsByClassName("mySlides");
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
      }
      myIndex++;
      if (myIndex > x.length) {myIndex = 1}    
      x[myIndex-1].style.display = "block";  
      setTimeout(carousel, 1000); // Change image every 1 second
    }
    </script>

  <!-- Contenu principal -->
  <div class="products">
  <div class="section">
      <h2>Meilleures ventes</h2>
      <p>Découvrez notre sélection des produits les plus vendus</p>
  </div>
  <div class="items">
      <?php
      // Connexion à la base de données (exemple avec MySQLi)
      $conn = new mysqli('localhost', 'root', '', 'ecommerce');

      // Vérification de la connexion
      if ($conn->connect_error) {
          die("Échec de la connexion à la base de données : " . $conn->connect_error);
      }

      // Requête pour récupérer les produits depuis la base de données
      $query = "SELECT * FROM produits";
      $result = $conn->query($query);

      // Affichage des produits sur la page d'accueil
      if ($result->num_rows > 0) {
          while ($row = $result->fetch_assoc()) {
              $nomProduit = $row['nom'];
              $description = $row['description'];
              // Autres champs du produit

              echo '<div class="item">';
              echo '<a href="#"><img src="#" alt=""></a>';
              echo '<a href="#"><h3>' . $nomProduit . '</h3></a>';
              echo '<p>' . $description . '</p>';
              echo '</div>';
          }
      } else {
          echo 'Aucun produit trouvé.';
      }

      // Fermeture de la connexion à la base de données
      $conn->close();
      ?>
  </div>
</div>

<section id="tranding">
  <div class="container">
    <h1 class="text-center section-heading">popular products</h1>
    <h3 class="text-center section-subheading">- popular  -</h3>
  </div>
  <div class="container">
    <div class="swiper tranding-slider">
      <div class="swiper-wrapper">
        <?php
        // Connexion à la base de données (exemple avec MySQLi)
        $conn = new mysqli('localhost', 'root', '', 'ecommerce');

        // Vérification de la connexion
        if ($conn->connect_error) {
          die("Échec de la connexion à la base de données : " . $conn->connect_error);
        }

        // Requête pour récupérer les produits depuis la base de données
        $query = "SELECT * FROM produits";
        $result = $conn->query($query);

        // Affichage des produits dans les slides
        if ($result->num_rows > 0) {
          while ($row = $result->fetch_assoc()) {
            $nomProduit = $row['nom'];
            $prixProduit = $row['prix1'];
            $noteProduit = $row['prix2'];
            $imageProduit = $row['image'];

            // Affichage du slide avec les données du produit
            echo '<div class="swiper-slide tranding-slide">';
            echo '<div class="tranding-slide-img">';
            echo '<img src="' . $imageProduit . '" alt="Tranding">';
            echo '</div>';
            echo '<div class="tranding-slide-content">';
            echo '<h1 class="Products-price">' . $prixProduit . '&nbsp;MAD</h1>';
            echo '<div class="tranding-slide-content-bottom">';
            echo '<h2 class="Products-name">' . $nomProduit . '</h2>';
            echo '<h3 class="products-rating">';
          

            echo '</div>';
            echo '</h3>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
          }
        } else {
          echo 'Aucun produit trouvé.';
        }

        // Fermeture de la connexion à la base de données
        $conn->close();
        ?>
      </div>

      <div class="tranding-slider-control">
        <div class="swiper-button-prev slider-arrow">
          <ion-icon name="arrow-back-outline"></ion-icon>
        </div>
        <div class="swiper-button-next slider-arrow">
          <ion-icon name="arrow-forward-outline"></ion-icon>
        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div>
  </div>
</section>


  <!-- Pied de page -->
  
  <footer>
    <div class="container">
      <div>
        <h2>Shop</h2>
        <ul>
          <li><a href="page-acceuil.php">Store</a></li>
          <li><a href="#">Gift</a></li>
          <li><a href="#">Student Discount</a></li>
        </ul>
      </div>
      <div>
      </div>
      <div>
        <h2>Quick Links</h2>
        <ul>
          <li><a href="contact.php">Contact us</a></li>
          <li><a href="termesetconditions.php">Terms & Condition</a></li>
        </ul>
      </div>
      <div>
        <h2>Newsletter</h2>
        <div>
          <input type="text" placeholder="Enter your text" />
          <button>Search</button>
        </div>
        <div>
          <ul>
            <li><span class="bi bi-telegram"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-instagram"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-linkedin"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-facebook"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-twitter"></span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          
        </div>
      </div>
    </div>
  </footer>
</body>
</html>