<!DOCTYPE HTML>
<html>
<head>
    <title>Sidebar / Header</title>
    
    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
        /* ---------- HEADER ---------- */
        .header {
            height: 70px;
            background-color: #ffffff;
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 0 30px 0 30px;
            box-shadow: 0 6px 7px -4px rgba(0, 0, 0, 0.2);
        }

        .menu-icon {
            display: none;
        }

        /* ---------- SIDEBAR ---------- */
        #sidebar {
            position: fixed;
            left: 0;
            top: 0;
            height: 100vh;
            width: 250px;
            background-color: #d8af6e;
            color: chocolate;
            font-size: 18px;
            overflow-y: auto;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
        }

        .sidebar-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 20px 20px 20px 20px;
            margin-bottom: 30px;
        }

        .sidebar-title > span {
            display: none;
        }

        .sidebar-brand {
            margin-top: 15px;
            font-size: 20px;
            font-weight: 700;
        }

        .sidebar-list {
            padding: 0;
            margin-top: 15px;
            list-style-type: none;
        }

        .sidebar-list-item {
            padding: 20px 20px 20px 20px;
        }

        .sidebar-list-item:hover {
            background-color: rgba(255, 255, 255, 0.2);
            cursor: pointer;
        }

        .sidebar-list-item > a {
            text-decoration: none;
            color: chocolate;
        }

        .sidebar-responsive {
            display: inline !important;
            position: absolute;
            /*
            the z-index of the ApexCharts is 11
            we want the z-index of the sidebar higher so that
            the charts are not showing over the sidebar 
            on small screens
            */
            z-index: 12 !important;
        }

        /* Custom CSS for center content */
        .center-content {
            margin: 0 0 0 250px;
            padding: 20px;
            max-width: calc(100% - 250px);
        }
        .active-link {
    color: chocolate;
    text-decoration: none;
    }

    </style>
</head>
<body>
    <!-- Header -->
    <header class="header">
        <div class="menu-icon" onclick="openSidebar()">
            <span class="material-icons-outlined">menu</span>
        </div>
        <div class="header-left">
            <span class="material-icons-outlined">search</span>
        </div>
        <div class="header-right">
            <span class="material-icons-outlined"><a href="admin-ordres.php">notifications</a></span>
            <span class="material-icons-outlined"><a href="admin-messages.php">email</a></span>
            <span class="material-icons-outlined">account_circle</span>
        </div>
    </header>
    <!-- End Header -->

    <!-- Sidebar -->
    <aside id="sidebar">
        <div class="sidebar-title">
            <div class="sidebar-brand">
            <span class="material-icons-outlined"></span>
            <a href="page-acceuil.php" class="active-link">OLIVIA</a>
            </div>
            <span class="material-icons-outlined" onclick="closeSidebar()">close</span>
        </div>

        <ul class="sidebar-list">
            <li class="sidebar-list-item">
                <a href="dashboard-admin.php" target="_blank">
                    <span class="material-icons-outlined">dashboard</span> &nbsp; &nbsp;&nbsp;Dashboard
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="admin-produits.php" target="_blank">
                    <span class="material-icons-outlined">inventory_2</span>&nbsp; &nbsp;&nbsp; Produits
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="liste-utilisateurs.php" target="_blank">
                    <span class="material-icons-outlined">groups_2</span>&nbsp; &nbsp;&nbsp;&nbsp;Utilisateurs
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="admin-comptes.php" target="_blank">
                    <span class="material-icons-outlined">person</span> &nbsp; &nbsp;&nbsp;Activation des &nbsp; &nbsp;&nbsp;&nbsp;
                     &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;comptes
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="admin-ordres.php" target="_blank">
                    <i class="fas fa-boxing"></i>
                    <span class="material-icons-outlined">inventory_2</span> &nbsp; &nbsp;&nbsp;Ordres
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="admin-statistiques.php" target="_blank">
                    <span class="material-icons-outlined">poll</span> &nbsp; &nbsp;&nbsp;Statistiques
                </a>
            </li>
            <li class="sidebar-list-item">
                <a href="admin-messages.php" target="_blank">
                    <span class="material-icons-outlined">messages</span>Messages
                </a>
            </li>
           
        </ul>
    </aside>
    <!-- End Sidebar -->

    <!-- JavaScript to toggle sidebar -->
    <script>
        function openSidebar() {
            document.getElementById("sidebar").style.width = "250px";
        }

        function closeSidebar() {
            document.getElementById("sidebar").style.width = "0";
        }
    </script>
</body>
</html>
