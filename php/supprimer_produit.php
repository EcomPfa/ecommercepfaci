<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ecommerce";

// Connexion à la base de données
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Échec de la connexion à la base de données: " . $conn->connect_error);
}

// Récupération des données du formulaire
$idPanier = $_POST['id_panier'];
$idProduit = $_POST['id_produit'];

// Suppression du produit de la table detail_panier
$sql = "DELETE FROM detail_panier WHERE id_panier = $id_panier AND id_produit = $id_produit";
$result = $conn->query($sql);
if ($result) {
    echo "Produit supprimé avec succès.";
} else {
    echo "Erreur lors de la suppression du produit : " . $conn->error;
}

// Fermer la connexion à la base de données
$conn->close();
?>

