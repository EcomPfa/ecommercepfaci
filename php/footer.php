<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Footer</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        /* Styles pour le pied-page*/
        footer{
            
            width: 100%;
            background-color: #d8af6e;
        }
        .container{
            display: flex;
            align-items: baseline;
            justify-content: space-around;
            width: 90%;
            margin: 20px auto 20px;
        }
        .container>div>h2{
            color: #000000;
            font-family: sans-serif;
            font-size: 24px;
            padding: 20px 0;
            user-select: none;
        }
        .container>div>ul{
            list-style-type: none;
        }
        .container>div>ul>li{
            margin: 16px 0;
            cursor: pointer;
        }
        .container>div>ul>li>a{
            font-size: 20px;
            text-decoration: none;
            font-family: sans-serif;
            position: relative;
            left: 0;
            padding: 2px;
            color: #ececeb;
            font-weight: 700;
            transition: all 0.4s;
        }
        .container>div>ul>li>a:hover{
            left: 8px;
        }
        .container>div>div{
            margin: 20px 0 10px;
            padding: 6px 14px;
            border-radius: 7px;
            width: 100%;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
        }
        .container>div>div>input{
            color: #000;
            background-color: #fff;
            padding: 16px 10px;
            border-radius: 4px;
            margin-right: 10px;
            border: none;
            outline: none;
        }
        .container>div>div>button{
            padding: 16px 26px;
            font-size: 14px;
            border: none;
            outline: none;
            border-radius: 4px;
            background-color: #000000;
            color: #fff;
            text-transform: uppercase;
            cursor: pointer;
        }
        .container>div>div>ul{
            list-style-type: none;
        }
        .container>div>div>ul>li{
            display: inline-block;
            margin: 0 4px;
        }
        .container>div>div>ul>li>span{
            background-color: #000000;
            padding: 14px 14px;
            font-size: 20px;
            color: #fff;
            border-radius: 50%;
        }
    </style>
</head>
<body>
<footer>
    <div class="container">
        <div>
            <h2>Shop</h2>
            <ul>
                <li><a href="page-acceuil.php">Store</a></li>
                <li><a href="#">Gift</a></li>
                <li><a href="#">Student Discount</a></li>
            </ul>
        </div>
        <div>
        </div>
        <div>
            <h2>Quick Links</h2>
            <ul>
                <li><a href="contact.php">Contact us</a></li>
                <li><a href="termesetconditions.php">Terms & Condition</a></li>
            </ul>
        </div>
        <div>
            <h2>Newsletter</h2>
            <div>
                <input type="text" placeholder="Enter your text" />
                <button>Search</button>
            </div>
            <div>
                <ul>
                    <li><span class="bi bi-telegram"></span></li>
                </ul>
                <ul>
                    <li><span class="bi bi-instagram"></span></li>
                </ul>
                <ul>
                    <li><span class="bi bi-linkedin"></span></li>
                </ul>
                <ul>
                    <li><span class="bi bi-facebook"></span></li>
                </ul>
                <ul>
                    <li><span class="bi bi-twitter"></span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            </div>
        </div>
    </div>
</footer>
</body>
</html>
