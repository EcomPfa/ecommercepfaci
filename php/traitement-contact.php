<?php
// Vérifier si le formulaire a été soumis
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // Récupérer les valeurs des champs du formulaire
  $nom_complet = $_POST['nom_complet'];
  $email = $_POST['email'];
  $telephone = $_POST['telephone'];
  $message = $_POST['message'];

  // Valider les données si nécessaire
  // ...

  // Effectuer la connexion à la base de données (remplacez les valeurs par les vôtres)
  $host = 'localhost';
  $dbname = 'ecommerce';
  $username = 'root';
  $password = '';

  try {
    $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Préparer la requête d'insertion des données dans la table "messages"
    $query = "INSERT INTO messages (nom_complet, email, telephone, message) VALUES (:nom_complet, :email, :telephone, :message)";
    $statement = $db->prepare($query);

    // Exécuter la requête avec les valeurs des champs du formulaire
    $statement->execute([
      ':nom_complet' => $nom_complet,
      ':email' => $email,
      ':telephone' => $telephone,
      ':message' => $message
    ]);

    // Rediriger l'utilisateur vers une page de confirmation ou afficher un message de succès
    // ...

  } catch (PDOException $e) {
    // Gérer les erreurs de connexion à la base de données
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
  }
}
?>
