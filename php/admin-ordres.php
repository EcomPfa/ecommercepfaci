<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Connexion à la base de données
$host = 'localhost';
$dbname = 'ecommerce';
$username = 'root';
$password = '';

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
    exit();
}

include "inc/functions.php";

// Récupérer les commandes confirmées
$commandes = getCommandesConfirmes($conn);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-Shop</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <style>
     body {
            font-family: Arial, sans-serif;
            background-color: white;
            color: black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid black;
        }

        th {
            background-color: chocolate;
            color: white;
            font-weight: bold;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        h1 {
            color: chocolate;
            text-align: center;
        }

        .search-bar {
            display: flex;
            justify-content: left;
            margin-bottom: 10px;
        }

        input[type="text"][name="query"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        button[type="submit"] {
            padding: 10px 20px;
            background-color: chocolate;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
         
        }

        button[type="submit"]:hover {
            background-color: #8b4513;
        }

        button[type="submit"]:focus {
            outline: none;
        }

        button[type="submit"]:active {
            background-color: #8b4513;
        }body {
            font-family: Arial, sans-serif;
            background-color: white;
            color: black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid black;
        }

        th {
            background-color: chocolate;
            color: white;
            font-weight: bold;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        h1 {
            color: chocolate;
            text-align: center;
        }

        .search-bar {
            display: flex;
            justify-content: left;
            margin-bottom: 10px;
        }

        input[type="text"][name="query"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        button[type="submit"] {
            padding: 10px 20px;
            background-color: chocolate;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
         
        }

        button[type="submit"]:hover {
            background-color: #8b4513;
        }

        button[type="submit"]:focus {
            outline: none;
        }

        button[type="submit"]:active {
            background-color: #8b4513;
        }body {
            font-family: Arial, sans-serif;
            background-color: white;
            color: black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid black;
        }

        th {
            background-color: chocolate;
            color: white;
            font-weight: bold;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        h1 {
            color: chocolate;
            text-align: center;
        }

        .search-bar {
            display: flex;
            justify-content: left;
            margin-bottom: 10px;
        }

        input[type="text"][name="query"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        button[type="submit"] {
            padding: 10px 20px;
            background-color: chocolate;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
         
        }

        button[type="submit"]:hover {
            background-color: #8b4513;
        }

        button[type="submit"]:focus {
            outline: none;
        }

        button[type="submit"]:active {
            background-color: #8b4513;
        }

    </style>
</head>

<body>
    <?php include("sidebar-header.php"); ?>
    <div class="container">
        <br><br><br>
        <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Commandes Confirmées</h1>
        <br> <br><div class="center-content">
        <table class="table">
            <thead >
                <tr>
                    <th  scope="col">ID Client</th>
                    <th scope="col">ID Vendeur</th>
                    <th scope="col">ID Produit</th>
                    <th scope="col">Prix Total</th>
                    <th scope="col">Quantité</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($commandes as $commande) {
                    echo '<tr>';
                    echo '<td>' . $commande['id_client'] . '</td>';
                    echo '<td>' . $commande['id_vendeur'] . '</td>';
                    echo '<td>' . $commande['id_produit'] . '</td>';
                    echo '<td>' . $commande['prix_total'] . '</td>';
                    echo '<td>' . $commande['quantite'] . '</td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
            </div>
</body>
</html>
