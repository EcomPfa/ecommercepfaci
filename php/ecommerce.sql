-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : dim. 18 juin 2023 à 14:38
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ecommerce`
--

-- --------------------------------------------------------

--
-- Structure de la table `32`
--

CREATE TABLE `32` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE `administrateur` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom_complet` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`id`, `nom_complet`, `email`, `mdp`, `role`) VALUES
(2, '', 'admin@gmail.com', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_modification` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `description`, `date_modification`) VALUES
(14, 'Epices et condiments', '', '0000-00-00'),
(15, 'Produits de la ruche', 'Miels', '0000-00-00'),
(16, 'Produits à base de dattes', '', '0000-00-00'),
(17, 'Pâtisseries traditionnelles', '', '0000-00-00'),
(18, 'Huiles', '', '0000-00-00'),
(19, 'Produits à base d´argan', '', '0000-00-00'),
(20, 'Boissons traditionnelles', '', '0000-00-00'),
(21, 'Produits du terroir divers', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `nom_complet` varchar(40) NOT NULL,
  `telephone` int(11) NOT NULL,
  `adresse` int(11) NOT NULL,
  `pays` varchar(40) NOT NULL,
  `ville` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mdp` varchar(20) NOT NULL,
  `role` varchar(10) NOT NULL DEFAULT 'client',
  `compte_active` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `nom_complet`, `telephone`, `adresse`, `pays`, `ville`, `email`, `mdp`, `role`, `compte_active`) VALUES
(1, 'test1', 0, 0, '', '', 'test1@gmail.com', 'test1', 'client', 1),
(6, 'test2', 0, 0, '', '', 'test2@gmail.com', 'test2', 'client', 1),
(8, 'test5', 0, 0, '', '', 'test5@gmail.com', 'test5', 'client', 0),
(11, 'test6', 0, 0, '', '', 'test6@gmail.com', 'test6', 'client', 0),
(12, 'nono', 0, 0, '', '', 'nono@gmail.com', 'nono', 'client', 1),
(13, 'test10', 0, 0, '', '', 'test10@gmail.com', 'test10', 'client', 0),
(14, 'nouha1', 0, 0, '', '', 'nouha1@gmail.com', 'nouha1', 'client', 0),
(15, 'nouha2', 0, 0, '', '', 'nouha2@gmail.com', 'nouha2', 'client', 0),
(16, 'nouha21', 0, 0, '', '', 'nouha21@gmail.com', 'nouha21', 'client', 0);

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE `commentaires` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `id_produit` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `date_creation` date NOT NULL DEFAULT current_timestamp(),
  `evaluation` int(11) NOT NULL,
  `photo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `detail_panier`
--

CREATE TABLE `detail_panier` (
  `id` int(11) NOT NULL,
  `id_panier` int(20) NOT NULL,
  `id_produit` int(20) NOT NULL,
  `quantite` int(20) NOT NULL,
  `pourcentage_promo` int(20) NOT NULL,
  `prix_unitaire` float NOT NULL,
  `tva` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `detail_panier`
--

INSERT INTO `detail_panier` (`id`, `id_panier`, `id_produit`, `quantite`, `pourcentage_promo`, `prix_unitaire`, `tva`) VALUES
(1, 8, 48, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `nom_complet` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `telephone` int(20) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`id`, `nom_complet`, `email`, `telephone`, `message`) VALUES
(1, 'test1', 'test1@gmail.com', 611223344, 'blablablabla');

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE `panier` (
  `id` int(11) NOT NULL,
  `id_vendeur` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `id_client` int(11) NOT NULL,
  `prix_total` float NOT NULL,
  `confirmation` tinyint(1) DEFAULT 0,
  `semaine` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`id`, `id_vendeur`, `date`, `id_client`, `prix_total`, `confirmation`, `semaine`) VALUES
(8, 2, '2023-06-17 15:07:18', 12, 100, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `prix1` float NOT NULL,
  `prix2` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `categorie` varchar(11) NOT NULL,
  `id_vendeur` int(11) NOT NULL,
  `date_creation` date NOT NULL,
  `date_modification` date NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `nom`, `description`, `prix1`, `prix2`, `image`, `categorie`, `id_vendeur`, `date_creation`, `date_modification`, `stock`) VALUES
(48, 'huile d\'avocat', 'lalalalalalalalala', 100, 0, '', 'huiles', 2, '0000-00-00', '0000-00-00', 50);

-- --------------------------------------------------------

--
-- Structure de la table `vendeurs`
--

CREATE TABLE `vendeurs` (
  `id` int(11) NOT NULL,
  `nom_complet` varchar(40) NOT NULL,
  `photo_profil` varchar(20) NOT NULL,
  `CIN` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mdp` varchar(40) NOT NULL,
  `date_creation` date NOT NULL,
  `nom_boutique` varchar(40) NOT NULL,
  `pourcentage_admin` int(10) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'vendeur',
  `compte_active` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `vendeurs`
--

INSERT INTO `vendeurs` (`id`, `nom_complet`, `photo_profil`, `CIN`, `email`, `mdp`, `date_creation`, `nom_boutique`, `pourcentage_admin`, `role`, `compte_active`) VALUES
(2, 'test4', '', 0, 'test4@gmail.com', 'test4', '0000-00-00', '', 0, 'vendeur', 0),
(3, 'test', '', 0, 'test@gmail.com', 'test', '0000-00-00', '', 0, 'vendeur', 0),
(4, 'nouha', '', 0, 'nouha@gmail.com', 'nouha', '0000-00-00', '', 0, 'vendeur', 0),
(5, 'dounia', '', 0, 'dounia@gmail.com', 'dounia', '0000-00-00', '', 0, 'vendeur', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `nom` (`nom`);

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_id` (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commentaires_clients` (`id_client`),
  ADD KEY `fk_commentaires_produits` (`id_produit`);

--
-- Index pour la table `detail_panier`
--
ALTER TABLE `detail_panier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detail_panier_panier` (`id_panier`),
  ADD KEY `fk_detail_panier_produits` (`id_produit`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD KEY `fk_messages_clients` (`email`);

--
-- Index pour la table `panier`
--
ALTER TABLE `panier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_panier_clients` (`id_client`),
  ADD KEY `fk__panier_vendeurs` (`id_vendeur`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `nom` (`nom`),
  ADD KEY `fk_produits_categories` (`categorie`),
  ADD KEY `fk_produits_vendeurs` (`id_vendeur`);

--
-- Index pour la table `vendeurs`
--
ALTER TABLE `vendeurs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE,
  ADD UNIQUE KEY `id` (`id`) USING BTREE;

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `administrateur`
--
ALTER TABLE `administrateur`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `detail_panier`
--
ALTER TABLE `detail_panier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `panier`
--
ALTER TABLE `panier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `vendeurs`
--
ALTER TABLE `vendeurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `fk_commentaires_clients` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `fk_commentaires_produits` FOREIGN KEY (`id_produit`) REFERENCES `produits` (`id`);

--
-- Contraintes pour la table `detail_panier`
--
ALTER TABLE `detail_panier`
  ADD CONSTRAINT `fk_detail_panier_panier` FOREIGN KEY (`id_panier`) REFERENCES `panier` (`id`),
  ADD CONSTRAINT `fk_detail_panier_produits` FOREIGN KEY (`id_produit`) REFERENCES `produits` (`id`);

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_clients` FOREIGN KEY (`email`) REFERENCES `clients` (`email`);

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `fk__panier_vendeurs` FOREIGN KEY (`id_vendeur`) REFERENCES `vendeurs` (`id`),
  ADD CONSTRAINT `fk_panier_clients` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id`);

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `fk_produits_categories` FOREIGN KEY (`categorie`) REFERENCES `categories` (`nom`),
  ADD CONSTRAINT `fk_produits_vendeurs` FOREIGN KEY (`id_vendeur`) REFERENCES `vendeurs` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
