<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Commander</title>
    <!-- Liens CSS -->
    <style>
        /* Votre style CSS */
    </style>
</head>
<body>
    <h1>Passer une commande</h1>

    <form action="traitement_commande.php" method="post">
        <label for="nom">Nom complet :</label>
        <input type="text" id="nom" name="nom" required><br><br>

        <label for="telephone">Téléphone :</label>
        <input type="tel" id="telephone" name="telephone" required><br><br>

        <label for="adresse">Adresse :</label>
        <input type="text" id="adresse" name="adresse" required><br><br>

        <label for="email">Adresse e-mail :</label>
        <input type="email" id="email" name="email" required><br><br>

        <label for="ville">Ville :</label>
        <input type="text" id="ville" name="ville" required><br><br>

        <label for="pays">Pays :</label>
        <input type="text" id="pays" name="pays" required><br><br>

        <input type="submit" value="Valider la commande">
    </form>

   <!-- Pied de page -->
  
   <footer>
    <div class="container">
      <div>
        <h2>Shop</h2>
        <ul>
          <li><a href="page-acceuil.php">Store</a></li>
          <li><a href="#">Gift</a></li>
          <li><a href="#">Student Discount</a></li>
        </ul>
      </div>
      <div>
      </div>
      <div>
        <h2>Quick Links</h2>
        <ul>
          <li><a href="contact.php">Contact us</a></li>
          <li><a href="termesetconditions.php">Terms & Condition</a></li>
        </ul>
      </div>
      <div>
        <h2>Newsletter</h2>
        <div>
          <input type="text" placeholder="Enter your text" />
          <button>Search</button>
        </div>
        <div>
          <ul>
            <li><span class="bi bi-telegram"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-instagram"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-linkedin"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-facebook"></span></li>
          </ul>
          <ul>
            <li><span class="bi bi-twitter"></span></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          
        </div>
      </div>
    </div>
  </footer>

</body>
</html>
