<?php
// Code pour se connecter à la base de données
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'ecommerce';

$conn = mysqli_connect($host, $username, $password, $database);
if (!$conn) {
  die('Erreur de connexion à la base de données : ' . mysqli_connect_error());
}

// Requête SQL pour récupérer les données des commandes avec l'état de confirmation
$commandesQuery = "SELECT semaine, COUNT(*) AS commandes, SUM(confirmation) AS commandes_confirmees FROM panier WHERE confirmation = 1 GROUP BY semaine";
$commandesResult = mysqli_query($conn, $commandesQuery);

// Créez un tableau pour stocker les données des commandes
$commandesData = array();

// Parcourez les résultats et ajoutez-les au tableau des données des commandes
while ($row = mysqli_fetch_assoc($commandesResult)) {
  $semaine = $row['semaine'];
  $commandes = $row['commandes'];
  $commandesConfirmees = $row['commandes_confirmees'];

  // Ajoutez les données à un tableau associatif
  $commande = array(
    'semaine' => $semaine,
    'commandes' => $commandes,
    'commandes_confirmees' => $commandesConfirmees
  );

  // Ajoutez l'élément au tableau des données des commandes
  $commandesData[] = $commande;
}

// Convertir les données en format JSON et renvoyez-les
echo json_encode($commandesData);

// Fermez la connexion à la base de données
mysqli_close($conn);
?>
