<?php
// Code pour se connecter à la base de données
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'ecommerce';

$conn = mysqli_connect($host, $username, $password, $database);
if (!$conn) {
  die('Erreur de connexion à la base de données : ' . mysqli_connect_error());
}

// Requête SQL pour récupérer les données des utilisateurs inscrits par semaine
$utilisateursQuery = "SELECT WEEK(date_inscription) AS semaine, COUNT(*) AS utilisateurs FROM (SELECT date_inscription FROM clients UNION ALL SELECT date_inscription FROM vendeurs) AS utilisateurs_table GROUP BY semaine";
$utilisateursResult = mysqli_query($conn, $utilisateursQuery);

// Créez un tableau pour stocker les données des utilisateurs
$utilisateursData = array();

// Parcourez les résultats et ajoutez-les au tableau des données des utilisateurs
while ($row = mysqli_fetch_assoc($utilisateursResult)) {
  $semaine = $row['semaine'];
  $utilisateurs = $row['utilisateurs'];

  // Ajoutez les données à un tableau associatif
  $utilisateur = array(
    'semaine' => $semaine,
    'utilisateurs' => $utilisateurs
  );

  // Ajoutez l'élément au tableau des données des utilisateurs
  $utilisateursData[] = $utilisateur;
}

// Convertir les données en format JSON et renvoyez-les
echo json_encode($utilisateursData);

// Fermez la connexion à la base de données
mysqli_close($conn);
?>
