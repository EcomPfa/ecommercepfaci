<!DOCTYPE html>
<html lang="en">
<head>
  <title>Création de la boutique</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="page-acceuil.php">
  <style>

    * {
      box-sizing: border-box;
      margin: 0;
      padding: 0;
    }

    body {
      background: #f6f5f7;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      font-family: 'Montserrat', sans-serif;
      height: 100vh;
    }

    form {
      background-color: #fff;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      padding: 0 50px;
      height: 100%;
      text-align: center;
      width: 100%;
    }

    input {
      background-color: #d8af6e;
      border: none;
      padding: 12px 15px;
      margin: 8px 0;
      width: 70%;
      height: 10%;
    }

    button.ghost {
      background-color: transparent;
    }

    button {
      background-color: #d8af6e;
      color: black;
      border: 1px solid #d8af6e;
      font-size: 16px;
      font-weight: bold;
      padding: 16px 32px;
      margin-top: 24px;
      letter-spacing: 1px;
      border-radius: 50px;
      border-color: chocolate;
    }

    button:hover {
      cursor: pointer;
      background-color: chocolate;
      color: #d8af6e;
    }

    .container {
      position: relative;
      overflow: hidden;
      width: 50%;
      height:50%;
      background-color: #fff;
      border-radius: 5px; /* Adjust the border-radius value to make the border smaller */
      box-shadow: 0 8px 24px chocolate, 0 8px 8px chocolate;
    }

  </style>
</head>

<body>


  <div class="container" id="container">
      <form action="creer-boutique.php" method="post" id="boutiqueForm" enctype="multipart/form-data" class="f">
        <h2>Création de la boutique</h2>
        <br> <br>
        <input type="text" id="boutiqueName" name="boutiqueName" placeholder="Nom de la boutique" required>
        <input type="text" id="cin" name="cin" placeholder="CIN" required>
        <input type="file" id="profilePic" name="profilePic" accept="image/*" placeholder="Photo de profil" required>
        <br>
        <button type="submit" id="boutiqueSubmitBtn">Créer la boutique</button>
      </form>
      <script>
        const roleSelect = document.getElementById("role");
        const inscriptionForm = document.getElementById("inscription");

        inscriptionForm.addEventListener("submit", function(e) {
          if (roleSelect.value === "vendeur") {
            e.preventDefault(); // Prevent form submission
            window.location.href = "creer-boutique.php";
          }
        });
      </script>
  </div>
  <?php
   // Code PHP pour traiter le formulaire de création de la boutique

   if ($_SERVER['REQUEST_METHOD'] === 'POST') {
     // Récupérer les données du formulaire
     $boutiqueName = $_POST['boutiqueName'];
     $cin = $_POST['cin'];
     
     // Traitement du fichier téléchargé
     $targetDir = "uploads/";
     $targetFile = $targetDir . basename($_FILES["profilePic"]["name"]);
     $uploadOk = 1;
     $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
     
     // Vérifier si le fichier image est réellement une image
     $check = getimagesize($_FILES["profilePic"]["tmp_name"]);
     if ($check !== false) {
       echo "Le fichier est une image - " . $check["mime"] . ".";
       $uploadOk = 1;
     } else {
       echo "Le fichier n'est pas une image.";
       $uploadOk = 0;
     }
     
     // Vérifier la taille du fichier
     if ($_FILES["profilePic"]["size"] > 500000) {
       echo "Désolé, votre fichier est trop volumineux.";
       $uploadOk = 0;
     }
     
     // Autoriser certains formats de fichiers
     $allowedExtensions = array("jpg", "jpeg", "png", "gif");
     if (!in_array($imageFileType, $allowedExtensions)) {
       echo "Désolé, seuls les fichiers JPG, JPEG, PNG et GIF sont autorisés.";
       $uploadOk = 0;
     }
     
     // Vérifier si $uploadOk est défini sur 0 par une erreur
     if ($uploadOk == 0) {
       echo "Désolé, votre fichier n'a pas été téléchargé.";
     } else {
       // Si tout est correct, essayer de télécharger le fichier
       if (move_uploaded_file($_FILES["profilePic"]["tmp_name"], $targetFile)) {
         echo "Le fichier " . basename($_FILES["profilePic"]["name"]) . " a été téléchargé.";
         // Effectuer les opérations supplémentaires de création de la boutique ici
       } else {
         echo "Une erreur s'est produite lors du téléchargement de votre fichier.";
       }
     }
   }
  ?>
</body>

</html>
