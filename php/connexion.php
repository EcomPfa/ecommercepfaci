<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start(); // Démarrer la session

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Récupérer les données du formulaire
    $email = $_POST['email'];
    $mdp = $_POST['mdp'];

    // Effectuer des validations supplémentaires si nécessaire

    // Connexion à la base de données
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ecommerce";

    $conn = new mysqli($servername, $username, $password, $dbname);

    // Vérifier la connexion
    if ($conn->connect_error) {
        die("La connexion a échoué : " . $conn->connect_error);
    }
// Vérifier si les informations de connexion correspondent à un administrateur
$sql_admin = "SELECT email FROM administrateur WHERE email = ? AND mdp = ?";
$stmt_admin = $conn->prepare($sql_admin);
$stmt_admin->bind_param("ss", $email, $mdp);
$stmt_admin->execute();
$result_admin = $stmt_admin->get_result();

if ($result_admin !== false && $result_admin->num_rows > 0) {
    // Les informations de connexion sont correctes pour un administrateur
    header("Location: dashboard-admin.php");
    exit();
}

// Vérifier les informations de connexion dans les tables clients et vendeurs
$sql = "SELECT email, mdp, role FROM clients WHERE email = ? AND mdp = ? UNION SELECT email, mdp, role FROM vendeurs WHERE email = ? AND mdp = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("ssss", $email, $mdp, $email, $mdp);
$stmt->execute();
$result = $stmt->get_result();

if ($result !== false && $result->num_rows > 0) {
    $data = $result->fetch_assoc();
    $email = $data['email'];
    $mdp = $data['mdp'];

    // Check if the user is a client
    $sql_clients = "SELECT role FROM clients WHERE email = ?";
    $stmt_clients = $conn->prepare($sql_clients);
    $stmt_clients->bind_param("s", $email);
    $stmt_clients->execute();
    $result_clients = $stmt_clients->get_result();

    if ($result_clients !== false && $result_clients->num_rows > 0) {
        $role = $result_clients->fetch_assoc()['role'];
        if ($role === 'client') {
            echo "Redirecting to page-acceuil.php"; // Debug statement
            header("Location: page-acceuil.php");
            exit();
        }
    }

    // Check if the user is a vendor
    $sql_vendeurs = "SELECT role FROM vendeurs WHERE email = ?";
    $stmt_vendeurs = $conn->prepare($sql_vendeurs);
    $stmt_vendeurs->bind_param("s", $email);
    $stmt_vendeurs->execute();
    $result_vendeurs = $stmt_vendeurs->get_result();

    if ($result_vendeurs !== false && $result_vendeurs->num_rows > 0) {
        $role = $result_vendeurs->fetch_assoc()['role'];
        if ($role === 'vendeur') {
            echo "Redirecting to dashboard-vendeur.php"; // Debug statement
            header("Location: dashboard-vendeur.php");
            exit();
        }
    }
} else {
    // Les informations de connexion sont incorrectes
    // Rediriger vers la page d'inscription/connexion
    echo "Redirecting to inscri-conn.php"; // Debug statement
    header("Location: inscri-conn.php");
    exit();
}

// Fermer la connexion
$conn->close();
}
?>

