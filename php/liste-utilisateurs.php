<!DOCTYPE html>
<html>
<head>
    <title>Liste des utilisateurs</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: white;
            color: black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid black;
        }

        th {
            background-color: chocolate;
            color: white;
            font-weight: bold;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        h1 {
            color: chocolate;
            text-align: center;
        }

        .search-bar {
            display: flex;
            justify-content: left;
            margin-bottom: 10px;
        }

        input[type="text"][name="query"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        button[type="submit"] {
            padding: 10px 20px;
            background-color: chocolate;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
         
        }

        button[type="submit"]:hover {
            background-color: #8b4513;
        }

        button[type="submit"]:focus {
            outline: none;
        }

        button[type="submit"]:active {
            background-color: #8b4513;
        }
    </style>
    <script>
  document.addEventListener('DOMContentLoaded', function() {
    var actionIcons = document.querySelectorAll('.action-icon');
    actionIcons.forEach(function(icon) {
      icon.addEventListener('click', function() {
        var description = this.nextElementSibling;
        if (description.style.display === 'none') {
          description.style.display = 'inline';
        } else {
          description.style.display = 'none';
        }
      });
    });
  });
</script>
</head>
<body>
    <?php include("sidebar-header.php"); ?>
<h1> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    Liste des utilisateurs</h1>
<br>
<div class="center-content">
<!-- Barre de recherche -->
<div class="search-bar">
    <form action="liste-utilisateurs.php" method="get">
        <input type="text" name="query" placeholder="Rechercher les utilisateurs">
        <button type="submit"><i class="fa fa-search">Rechercher</i></button>
    </form>
</div>

<br><br>
<table>
    <tr>
        <th>ID</th>
        <th>Nom Complet</th>
        <th>Rôle</th>
        <th>Email</th>
    </tr>
    <?php
    $server = "localhost";
    $user = "root";
    $password = "";
    $database = "ecommerce";

    // Connexion à la base de données
    $conn = mysqli_connect($server, $user, $password, $database);

    // Vérification de la connexion
    if (!$conn) {
        die("Erreur de connexion à la base de données : " . mysqli_connect_error());
    }

    // Traitement de la recherche
    $query = $_GET['query'] ?? '';

    if (!empty($query)) {
        $query = mysqli_real_escape_string($conn, $query);
        $searchQuery = "SELECT id, nom_complet, role, email FROM (SELECT id, nom_complet, role, email FROM clients UNION SELECT id, nom_complet, role, email FROM vendeurs) AS users WHERE nom_complet LIKE '%$query%'";
        $searchResult = mysqli_query($conn, $searchQuery);

        if (mysqli_num_rows($searchResult) > 0) {
            while ($row = mysqli_fetch_assoc($searchResult)) {
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['nom_complet'] . "</td>";
                echo "<td>" . $row['role'] . "</td>";
                echo "<td>" . $row['email'] . "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='4'>Aucun utilisateur trouvé</td></tr>";
        }
    } else {
        // Récupération de tous les utilisateurs
        $query = "SELECT id, nom_complet, role, email FROM clients UNION SELECT id, nom_complet, role, email FROM vendeurs";
        $result = mysqli_query($conn, $query);

        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['nom_complet'] . "</td>";
                echo "<td>" . $row['role'] . "</td>";
                echo "<td>" . $row['email'] . "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='4'>Aucun utilisateur trouvé</td></tr>";
        }
    }

    // Fermeture de la connexion à la base de données
    mysqli_close($conn);
    ?>
</table>
</div>
</body>
</html>
