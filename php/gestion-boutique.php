<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start(); // Start the session

// Database connection
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ecommerce";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Code PHP pour traiter le formulaire d'ajout de produit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // Récupérer les données du formulaire
  $nom = $_POST['nom'];
  $prix1 = $_POST['prix1'];
  $prix2 = $_POST['prix2'];
  $categorie = $_POST['categories'];
  $description = $_POST['description'];
}

  // Retrieve data from the "produits" table
$sql = "SELECT * FROM produits";
$result = $conn->query($sql);
?>


<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<style>
    @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css');

table {
  width: 100%;
  border-collapse: collapse;
}

.add-product-button {
            margin-bottom: 20px;
        }

        .product-table {
            margin-bottom: 20px;
        }
th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #ddd;
}

thead th {
  background-color: #f2f2f2;
}

.action-icon {
  display: inline-block;
  margin-right: 10px;
}

.action-icon i {
  margin-right: 5px;
}
button[type="submit"] {
    padding: 10px 20px;
    background-color: chocolate;
    color: white;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    font-size: 16px;
  }
  
  button[type="submit"]:hover {
    background-color: chocolate;
  }
  
  button[type="submit"]:focus {
    outline: none;
  }
  
  button[type="submit"]:active {
    background-color:chocolate;
  }
  .search-bar {
width: max-content;
  }
  input[type="text"][name="query"] {
    padding: 10px;
    width: 300px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  
  input[type="text"][name="query"]:focus {
    outline: none;
    border-color: chocolate;
  }
  h1{
     color: chocolate;
     text-align: center;
  }
  
  table {
    width: 100%;
    border-collapse: collapse;
  }

  th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
  }

  thead th {
    background-color: #f2f2f2;
  }
  .action-icon {
      display: inline-block;
      cursor: pointer;
      margin-right: 10px;
      position: relative;
    }

    .action-icon i {
      margin-right: 5px;
    }

   .action-description {
      position: absolute;
      top: -20px;
      left: 50%;
      transform: translateX(-50%);
      background-color: #333;
      color: #fff;
      padding: 5px 10px;
      border-radius: 5px;
      opacity: 0;
     transition: opacity 0.3s;
   }
    .action-icon:hover .action-description {
      opacity: 1;
    }

</style>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var actionIcons = document.querySelectorAll('.action-icon');
    actionIcons.forEach(function(icon) {
      icon.addEventListener('click', function() {
        var description = this.nextElementSibling;
        if (description.style.display === 'none') {
          description.style.display = 'inline';
        } else {
          description.style.display = 'none';
        }
      });
    });
  });
</script>
</head>
<body>
    <h1>PRODUITS</h1>
    <br><br>
    <div>
         <!-- Barre de recherche -->
         <div class="search-bar">
  <form action="recherche.php" method="get">
    <input type="text" name="query" placeholder="Rechercher les produits"> 
    <button type="submit"><i class="fa fa-search"></i></button>                               
  </form>
</div>

<br><br> 

<div class="add-product-button">
        <a href="ajouter-produit.php"><button type="submit">Ajouter un produit</button></a>
    </div>

    <table class="product-table">

<table>
        <thead>
          <tr>
            <th>Nom</th>
            <th>Catégorie</th>
            <th>Prix 1</th>
            <th>Prix 2</th>
            <th>Ordres</th>
            <th>Actions</th>
            <th>Date d'ajout</th>
          </tr>
        </thead>
        <tbody>
      
    <?php while ($row = $result->fetch_assoc()) : ?>
        <tr>
            <td><?php echo $row['nom']; ?></td>
            <td><?php echo $row['categorie']; ?></td>
            <td><?php echo $row['prix1']; ?></td>
            <td><?php echo $row['prix2']; ?></td>
            <td><?php echo $row['ordres']; ?></td>
            <td>
                <span class="action-icon">
                    <i class="fas fa-edit"></i>
                    <span class="action-description">Modifier</span>
                </span>
                &nbsp; &nbsp;
                <span class="action-icon">
                    <i class="fas fa-trash-alt"></i>
                    <span class="action-description">Supprimer</span>
                </span>
            </td>
            <td><?php echo $row['date_ajout']; ?></td>
        </tr>
    <?php endwhile; ?>
    </tbody>
    </table>
    <br><br>
   
    </div>
</body>
</html>

