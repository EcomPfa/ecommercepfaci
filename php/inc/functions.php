<?php

// Fonction pour se connecter à la base de données
function connectDB()
{
    $host = 'localhost';
    $dbname = 'ecommerce';
    $username = 'root';
    $password = '';

    try {
        $conn = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $e) {
        echo "Erreur de connexion à la base de données : " . $e->getMessage();
        exit();
    }
}

// Fonction pour récupérer les commandes confirmées
function getCommandesConfirmes()
{
    $conn = connectDB();

    try {
        $query = "SELECT p.id_client, p.id_vendeur, dp.id_produit, p.prix_total, dp.quantite
                  FROM panier p
                  INNER JOIN detail_panier dp ON p.id = dp.id_panier
                  WHERE p.confirmation = 1";
        $stmt = $conn->prepare($query);
        $stmt->execute();

        $commandes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $commandes;
    } catch (PDOException $e) {
        echo "Erreur lors de la récupération des commandes confirmées : " . $e->getMessage();
        return false;
    }
}


?>