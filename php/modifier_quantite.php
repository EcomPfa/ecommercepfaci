<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ecommerce";

// Connexion à la base de données
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Échec de la connexion à la base de données: " . $conn->connect_error);
}

// Récupération des données du formulaire
$id_panier = $_POST['id_panier'];
$id_produit = $_POST['id_produit'];
$quantite = $_POST['quantite'];

// Mise à jour de la quantité dans la table detail_panier
$sql = "UPDATE detail_panier SET quantite = $quantite WHERE id_panier = $id_panier AND id_produit = $id_produit";
$result = $conn->query($sql);
if ($result) {
    echo "Quantité modifiée avec succès.";
} else {
    echo "Erreur lors de la modification de la quantité : " . $conn->error;
}

// Fermer la connexion à la base de données
$conn->close();
?>

