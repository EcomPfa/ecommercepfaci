<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajout de produit</title>
    
</head>
<style>
  * {
      box-sizing: border-box;
      margin: 0;
      padding: 0;
    }
  h2{
    text-align: center;
  }
    body {
      background: #f6f5f7;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      font-family: 'Montserrat', sans-serif;
      height: 100vh;
    }

    form {
      background-color: #fff;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      padding: 0 50px;
      height: 100%;
      text-align: center;
      width: 100%;
    }

    input {
      background-color: #d8af6e;
      border: none;
      padding: 12px 15px;
      margin: 8px 0;
      width: 70%;
    
    }

    button.ghost {
      background-color: transparent;
    }

    button {
      background-color: #d8af6e;
      color: black;
      border: 1px solid #d8af6e;
      font-size: 16px;
      font-weight: bold;
      padding: 16px 32px;
      margin-top: 24px;
      letter-spacing: 1px;
      border-radius: 50px;
      border-color: chocolate;
    }

    button:hover {
      cursor: pointer;
      background-color: chocolate;
      color: #d8af6e;
    }

    .container {
      position: relative;
      overflow: hidden;
      width: 60%;
      height:89%;
      background-color: #fff;
      border-radius: 5px; /* Adjust the border-radius value to make the border smaller */
      box-shadow: 0 8px 24px chocolate, 0 8px 8px chocolate;
    }

  textarea{
    margin-bottom: 15px;
    border: 1px solid #ccc;
    border-radius: 4px;
    width: 70%;
    padding: 50px;
  }

  
  /* Style pour le select */
  select {
    width: 72%;
    padding: 5px;
    margin-bottom: 15px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  
  
</style>
<body>
    <div class="container" id="container">
       <form action="ajouter-produit.php" method="POST" id="ajouter-produit" enctype="multipart/form-data" >
        <br>
         <h2>AJOUTER UN PRODUIT</h2><br>
         <label>Nom :</label>
          <input type="text" id="nom" name="nom" placeholder="Saisissez le nom du produit..." required>
          <label>Prix 1 :</label>
          <input type="text" id="prix1" name="prix1" placeholder="Saisissez le prix du produit... " required> 
          <label>Prix 2 :</label>
          <input type="text" id="prix2" name="prix2" placeholder="Saisissez le nouveau prix si vous souhaitez faire une promotion sur le produit..." >
          <label>Stocks :</label>
          <input type="text" id="stock" name="stock" placeholder="Saisissez le numero total des produits..." >
          <label for="image">Images :</label>
          <input type="file" id="image" name="image" placeholder="" accept="image/*" required> 
          <label for="description">Description :</label>
          <textarea id="description" name="description" placeholder="" >
          </textarea>
          <label for="categories" style="margin-bottom: 10px;">Categories :</label>
<select class="my-select" id="categories" name="categories">
  <option value=" "></option>
  <option value="epices">EPICES ET CONDIMENTS</option>
  <option value="huiles">HUILES</option>
  <option value="patisserie">PATISSERIE TRADITIONNELLE</option>
  <option value="argan">PRODUITS A BASE D'ARGAN</option>
  <option value="dattes">PRODUITS A BASE DATTES</option>
  <option value="miels">PRODUITS DE LA RUCHE: MIELS</option>
  <option value="boissons">BOISSONS TRADITIONNELLES</option>
  <option value="divers">PRODUITS DE TERROIR DIVERS</option>
</select>


          <a href="gestion-boutique.php"><button type="submit">Enregistrer</button>
          </form>
       </div>
      
    <?php
    // Code PHP pour traiter le formulaire d'ajout de produit

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Récupérer les données du formulaire
        $nom = $_POST['nom'];
        $prix1 = $_POST['prix1'];
        $prix2 = $_POST['prix2'];
        $description = $_POST['description'];
        $categorie = $_POST['categories'];
        $stock=$_POST['stock'];

        // Traitement du fichier téléchargé
        $targetDir = "uploads/";
        $targetFile = $targetDir . basename($_FILES["image"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

        // Vérifier si le fichier image est réellement une image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if ($check !== false) {
            echo "Le fichier est une image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "Le fichier n'est pas une image.";
            $uploadOk = 0;
        }

        // Vérifier la taille du fichier
        if ($_FILES["image"]["size"] > 500000) {
            echo "Désolé, votre fichier est trop volumineux.";
            $uploadOk = 0;
        }

        // Autoriser certains formats de fichiers
        $allowedExtensions = array("jpg", "jpeg", "png", "gif");
        if (!in_array($imageFileType, $allowedExtensions)) {
            echo "Désolé, seuls les fichiers JPG, JPEG, PNG et GIF sont autorisés.";
            $uploadOk = 0;
        }

        // Vérifier si $uploadOk est défini sur 0 par une erreur
        if ($uploadOk == 0) {
            echo "Désolé, votre fichier n'a pas été téléchargé.";
        } else {
            // Si tout est correct, essayer de télécharger le fichier
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $targetFile)) {
                echo "Le fichier " . basename($_FILES["image"]["name"]) . " a été téléchargé.";
                // Effectuer les opérations supplémentaires d'ajout de produit ici
            } else {
                echo "Une erreur s'est produite lors du téléchargement de votre fichier.";
            }
        }
    }
    ?>  
</body>
</html>
