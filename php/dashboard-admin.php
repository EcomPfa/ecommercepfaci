<?php
// Effectuer la connexion à la base de données
$host = 'localhost';
$dbname = 'ecommerce';
$username = 'root';
$password = '';

try {
  $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", $username, $password);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Calculer le nombre total d'utilisateurs
  $query = "SELECT COUNT(*) AS totalUsers FROM (SELECT id FROM clients UNION ALL SELECT id FROM vendeurs) AS users";
  $statement = $db->query($query);
  $totalUsers = $statement->fetch(PDO::FETCH_ASSOC)['totalUsers'];
} catch (PDOException $e) {
  // Gérer les erreurs de connexion à la base de données
  echo "Erreur de connexion à la base de données : " . $e->getMessage();
}

try {
  $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", $username, $password);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Calculer le nombre total de messages
  $query = "SELECT COUNT(*) AS totalMessages FROM messages";
  $statement = $db->query($query);
  $totalMessages = $statement->fetch(PDO::FETCH_ASSOC)['totalMessages'];
} catch (PDOException $e) {
  // Gérer les erreurs de connexion à la base de données
  echo "Erreur de connexion à la base de données : " . $e->getMessage();
}

try {
  $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", $username, $password);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Calculer le nombre de produits
  $query = "SELECT COUNT(id) AS total FROM produits";
  $statement = $db->prepare($query);
  $statement->execute();
  $result = $statement->fetch(PDO::FETCH_ASSOC);
  $totalProducts = $result['total'];
} catch (PDOException $e) {
  // Gérer les erreurs de connexion à la base de données
  echo "Erreur de connexion à la base de données : " . $e->getMessage();
}
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Admin Dashboard</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-xxxx" crossorigin="anonymous" />


    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
        body {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  background-color: #e6e8ed;
  color: #666666;
  font-family: "Montserrat", sans-serif;
}

.material-icons-outlined {
  vertical-align: middle;
  line-height: 1px;
}

.text-primary {
  color: #666666;
}

.text-blue {
  color: #246dec;
}

.text-red {
  color: #cc3c43;
}

.text-green {
  color: #367952;
}

.text-orange {
  color: #f5b74f;
}

.font-weight-bold {
  font-weight: 600;
}

.grid-container {
  display: grid;
  grid-template-columns: 260px 1fr 1fr 1fr;
  grid-template-rows: 0.2fr 3fr;
  grid-template-areas:
    "sidebar header header header"
    "sidebar main main main";
  height: 100vh;
}


/* ---------- HEADER ---------- */

.header {
  grid-area: header;
  height: 70px;
  background-color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 30px 0 30px;
  box-shadow: 0 6px 7px -4px rgba(0, 0, 0, 0.2);
}

.menu-icon {
  display: none;
}


/* ---------- SIDEBAR ---------- */

#sidebar {
  grid-area: sidebar;
  height: 100%;
  background-color: #d8af6e;
  color: chocolate;
  font-size: 18px;
  overflow-y: auto;
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
}

.sidebar-title {

  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px 20px 20px 20px;
  margin-bottom: 30px;
}

.sidebar-title > span {
  display: none;
}

.sidebar-brand {
  margin-top: 15px;
  font-size: 20px;
  font-weight: 700;
}

.sidebar-list {
  padding: 0;
  margin-top: 15px;
  list-style-type: none;
}

.sidebar-list-item {
  padding: 20px 20px 20px 20px;
}

.sidebar-list-item:hover {
  background-color: rgba(255, 255, 255, 0.2);
  cursor: pointer;
}

.sidebar-list-item > a {
  text-decoration: none;
  color: chocolate;
}

.sidebar-responsive {
  display: inline !important;
  position: absolute;
  /*
    the z-index of the ApexCharts is 11
    we want the z-index of the sidebar higher so that
    the charts are not showing over the sidebar 
    on small screens
  */
  z-index: 12 !important;
}


/* ---------- MAIN ---------- */

.main-container {
  grid-area: main;
  overflow-y: auto;
  padding: 20px 20px;
}

.main-title {
  display: flex;
  justify-content: space-between;
}

.main-title > p {
  font-size: 20px;
}

.main-cards {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  gap: 20px;
  margin: 20px 0;
}

.card {
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding: 25px;
  background-color: #ffffff;
  box-sizing: border-box;
  border: 1px solid #d2d2d3;
  border-radius: 5px;
  box-shadow: 0 6px 7px -4px rgba(0, 0, 0, 0.2);
}

.card:first-child {
  border-left: 7px solid #246dec;
}

.card:nth-child(2) {
  border-left: 7px solid #f5b74f;
}

.card:nth-child(3) {
  border-left: 7px solid #367952;
}

.card:nth-child(4) {
  border-left: 7px solid #cc3c43;
}

.card > span {
  font-size: 20px;
  font-weight: 600;
}

.card-inner {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.card-inner > p {
  font-size: 18px;
}

.card-inner > span {
  font-size: 35px;
}

.charts {
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 20px;
}

.charts-card {
  background-color: #ffffff;
  margin-bottom: 20px;
  padding: 25px;
  box-sizing: border-box;
  -webkit-column-break-inside: avoid;
  border: 1px solid #d2d2d3;
  border-radius: 5px;
  box-shadow: 0 6px 7px -4px rgba(0, 0, 0, 0.2);
}

.chart-title {
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 22px;
  font-weight: 600;
}


/* ---------- SCROLLBARS ---------- */

::-webkit-scrollbar {
  width: 5px;
  height: 6px;
}

::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px #a5aaad;
  border-radius: 10px;
}

::-webkit-scrollbar-thumb {
  background-color: #4f35a1;
  border-radius: 10px;
}

::-webkit-scrollbar-thumb:hover {
  background-color: #a5aaad;
}


/* ---------- MEDIA QUERIES ---------- */


/* Medium <= 992px */
@media screen and (max-width: 992px) {
  .grid-container {
    grid-template-columns: 1fr;
    grid-template-rows: 0.2fr 3fr;
    grid-template-areas:
      "header"
      "main";
  }

  #sidebar {
    display: none;
  }

  .menu-icon {
    display: inline;
  }

  .sidebar-title > span {
    display: inline;
  }
}

/* Small <= 768px */
@media screen and (max-width: 768px) {
  .main-cards {
    grid-template-columns: 1fr;
    gap: 10px;
    margin-bottom: 0;
  }

  .charts {
    grid-template-columns: 1fr;
    margin-top: 30px;
  }
}

/* Extra Small <= 576px */
@media screen and (max-width: 576px) {
  .header-left {
    display: none;
  }
}
.active-link {
    color: chocolate;
    text-decoration: none;
    }
        </style>
  </head>
  <body>
    <div class="grid-container">

      <!-- Header -->
      <header class="header">
        <div class="menu-icon" onclick="openSidebar()">
          <span class="material-icons-outlined">menu</span>
        </div>
        <div class="header-left">
          <span class="material-icons-outlined">search</span>
        </div>
        <div class="header-right">
        <span class="material-icons-outlined"><a href="admin-ordres.php">notifications</a></span>
            <span class="material-icons-outlined"><a href="admin-messages.php">email</a></span>
          <span class="material-icons-outlined">account_circle</span>
        </div>
      </header>
      <!-- End Header -->

      <!-- Sidebar -->
      <aside id="sidebar">
        <div class="sidebar-title">
          <div class="sidebar-brand">
          <span class="material-icons-outlined"></span>
          <a href="page-acceuil.php" class="active-link">OLIVIA</a>
          </div>
          <span class="material-icons-outlined" onclick="closeSidebar()">close</span>
        </div>

        <ul class="sidebar-list">
          <li class="sidebar-list-item">
            <a href="dashboard-admin.php" target="_blank">
              <span class="material-icons-outlined">dashboard</span> &nbsp; &nbsp;&nbsp;Dashboard
            </a>
          </li>
          <li class="sidebar-list-item">
            <a href="admin-produits.php" target="_blank">
              <span class="material-icons-outlined">inventory_2</span>&nbsp; &nbsp;&nbsp; Produits
            </a>
          </li>
          <li class="sidebar-list-item">
            <a href="liste-utilisateurs.php" target="_blank">
              <span class="material-icons-outlined">
                groups_2
                </span>&nbsp; &nbsp;&nbsp;&nbsp;Utilisateurs
            </a>
          </li>
          <li class="sidebar-list-item">
            <a href="admin-comptes.php" target="_blank">
              <span class="material-icons-outlined">
                person
                </span> &nbsp; &nbsp;&nbsp;Activation des &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;comptes
            </a>
          </li>
          <li class="sidebar-list-item">
            <a href="admin-ordres.php" target="_blank">
            <i class="fas fa-boxing"></i>
              <span class="material-icons-outlined">inventory_2</span> &nbsp; &nbsp;&nbsp;Ordres
            </a>
          </li>
          <li class="sidebar-list-item">
            <a href="admin-statistiques.php" target="_blank">
              <span class="material-icons-outlined">poll</span> &nbsp; &nbsp;&nbsp;Statistiques
            </a>
          </li>
          <li class="sidebar-list-item">
            <a href="admin-messages.php" target="_blank">
              <span class="material-icons-outlined">messages</span>Messages
            </a>
          </li>
        
        </ul>
      </aside>
      <!-- End Sidebar -->

      <!-- Main -->
      <main class="main-container">
        <div class="main-title">
          <p class="font-weight-bold">DASHBOARD</p>
        </div>

        <div class="main-cards">

          <div class="card">
            <a href="admin-produits.php">
            <div class="card-inner">
              <p class="text-primary">PRODUITS</p>
              </a>
              <span class="material-icons-outlined text-blue">inventory_2</span>
            </div>
            <span class="text-primary font-weight-bold"><?php echo $totalProducts; ?></span>
          </div>  

          <div class="card">
          <a href="liste-utilisateurs.php">
            <div class="card-inner">
              <p class="text-primary"><a href="liste-utilisateurs.php">UTILISATEURS</p>
          </a>
              <span class="material-icons-outlined text-orange">
                groups_2
                </span>
            </div>
            <span class="text-primary font-weight-bold"> <?php echo $totalUsers; ?></span>
          </div>
        
          

          <div class="card">
            <a href="admin-comptes.php">
            <div class="card-inner">
              <p class="text-primary">ACTIVATION DES COMPTES</p>
              </a>
              <span class="material-icons-outlined text-green">
                person
                </span>
            </div>
            <span class="text-primary font-weight-bold"><?php echo $totalUsers; ?></span>
          </div>

          <div class="card">
           <a href="admin-messages.php">
            <div class="card-inner">
              <p class="text-primary">MESSAGES</p>
           </a>
            <span class="material-icons-outlined text-red">messages</span>
            </div>
            <span class="text-primary font-weight-bold"> <?php echo $totalMessages; ?> </span>
          </div>

        </div>

        <div class="charts">

          <div class="charts-card">
            <p class="chart-title">Top 5 Products</p>
            <div id="bar-chart"></div>
          </div>

          <div class="charts-card">
            <p class="chart-title">Les commerçants et les clients sont inscrits</p>
            <div id="area-chart"></div>
          </div>

        </div>
      </main>
      <!-- End Main -->

    </div>

    <!-- Scripts -->
    <!-- ApexCharts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/apexcharts/3.35.3/apexcharts.min.js"></script>
    <!-- Custom JS -->
    <script src="js/scripts.js"></script>
  </body>
</html>