<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet"type="text/css" title="style" href="termesetconditions.css"/>
  <title>Termes et conditions</title>
  <style>
    /*termes et conditions*/
body{
    margin:0;
    padding:0;
    background-color: #d8af6e ; 
    min-height:100vh;
    display:flex;
    align-items:center;
    justify-content:center;
}
h2{
   text-align:center;
}
.termes-box{
   max-width: 600px;
   background-color:black;
   color: #fff;
   font-family:"Montserrat";
   padding:60px 30px;
}
.termes-text{
   padding:0 20px;
   height:400px;
   overflow-y:auto;
   font-size:14px;
   font-weight:500;
   color:white;
}
.termes-text::-webkit-scrollbar{
   width:3px;
   background-color:black;
}
.termes-text::-webkit-scrollbar-thumb{
   background-color:#d8af6e;
}
.termes-text h2 {
text-transform:uppercase;
}
.termes-box h4{
   font-size:13px;
   text-align:center;
   padding:0 40 px;
}
b{
color:#d8af6e;
}
.termes-box h4 span{
   color:#d8af6e;
}
.buttons {
   display: flex;
   justify-content: center;
   align-items: center;
   margin-top: 20px;
 }
 
 .buttons a {
   text-decoration: none;
   margin: 0 10px;
 }
 
 .btn {
   padding: 10px 20px;
   border: none;
   border-radius: 5px;
   cursor: pointer;
   font-size: 14px;
   font-weight: ariel;
   text-transform: uppercase;
 }
 
 .green-btn {
   background-color: #4CAF50;
   color: #ffffff;
 }
 
 .grey-btn {
   background-color: #808080;
   color: #ffffff;
 }

.btn-hover{
opacity: .6;
}
  </style>
</head>

<body>
<div class ="termes-box">
<div class ="termes-text">
<h2> Termes & Conditions</h2>
<p>Bienvenue sur notre plateforme, nos précieux utilisateurs </p>

<p> <i><b><h4>Utilisation du site web:</b></i></h4><br>
    En utilisant notre site web, vous acceptez nos termes et conditions.</p>
	
<p><i><b><h4>Limitations de responsabilité:</b></i></h4><br>
   Nous ne sommes pas responsables des dommages causés par l'utilisation de notre site web.</p>	
	
<p><i><b><h4>Commande:</b></i></h4> <br> 
   Le client a la possibilité de passer commande en ligne sur notre site. La commande ne peut être enregistrée sur le site que si le client
   s'est clairement identifié par l'entrée de son adresse e-mail et de son mot de passe qui lui sont strictement personnels.
   La confirmation de la commande entraîne acceptation des présentes conditions de vente.
</p>

<p><i><b><h4>Paiement:</b></i></h4><br>
   Le règlement s'effectue lors de la réception de la commande.
</p>

<p><i><b><h4>Livraison:</b></i></h4><br>
   Les produits commandés sont livrés à l'adresse de livraison indiquée par le client lors de la commande. 
   Le délai de livraison est de 10 jours ouvrables.
</p>

<p><i><b><h4>Droit de rétractation:</b></i></h4><br>
   Le client dispose d'un délai de 20 jours pour retourner le produit qui ne lui conviendrait pas. 
   Les frais de retour seront à sa charge.

<p><i><b><h4>Garanties:</h4></b></i><br>
   Tous les produits fournis par le site bénéficient de la garantie légale prévue par les articles 1641 et suivants du Code civil.
   En cas de non-conformité d'un produit vendu, il pourra être retourné au vendeur qui le reprendra, l'échangera ou le remboursera.
</p>
 
<p><i><b><h4>Modification des termes et conditions:</h4></b></i><br>
   Nous nous réservons le droit de modifier ces termes et conditions à tout moment.</p>
  
<p><i><b><h4>Contactez-nous</h4></b></i><br>
   Si vous avez des questions ou des préoccupations concernant nos termes et conditions, veuillez nous contacter.</p>
  
<p> <i>Date de la dernière mise à jour :</i> 14/05/2023 </p>  
</div>
<h4>Je comprends et j'accepte<span> les termes et conditions </span>
&nbsp;et la politique de confidentialité en vigueur pour l'utilisation de ce site.
</h4>
<div class="buttons">
<a href="page-acceuil.php"><button class="btn green-btn">Accepter</button></a>
<a href="https://www.google.com/"><button class ="btn grey-btn">Refuser</button></a>
</div>
</body>
</html>
