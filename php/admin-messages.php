<?php
// Effectuer la connexion à la base de données
$host = 'localhost';
$dbname = 'ecommerce';
$username = 'root';
$password = '';

try {
    $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8mb4", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Récupérer les messages depuis la table "messages"
    $query = "SELECT * FROM messages";
    $statement = $db->prepare($query);
    $statement->execute();
    $messages = $statement->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    // Gérer les erreurs de connexion à la base de données
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <title>Messages des utilisateurs</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: white;
            color: black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid black;
        }

        th {
            background-color: chocolate;
            color: white;
            font-weight: bold;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        h1 {
            color: chocolate;
            text-align: center;
        }

        .search-bar {
            display: flex;
            justify-content: left;
            margin-bottom: 10px;
        }

        input[type="text"][name="query"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        button[type="submit"] {
            padding: 10px 20px;
            background-color: chocolate;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
        }

        button[type="submit"]:hover {
            background-color: #8b4513;
        }

        button[type="submit"]:focus {
            outline: none;
        }

        button[type="submit"]:active {
            background-color: #8b4513;
        }
    </style>
</head>

<body>
    <?php include("sidebar-header.php"); ?>

    <h1> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Les messages des utilisateurs</h1>
    <div class="center-content">
    <!-- Barre de recherche -->
    <div class="search-bar">
        <form action="liste-utilisateurs.php" method="get">
            <input type="text" name="query" placeholder="Rechercher les utilisateurs">
            <button type="submit"><i class="fa fa-search">Rechercher</i></button>
        </form>
    </div>
<br><br>
    <table>
        <tr>
            <th>ID</th>
            <th>Nom complet</th>
            <th>Email</th>
            <th>Téléphone</th>
            <th>Message</th>
        </tr>

        <?php foreach ($messages as $message) : ?>
            <tr>
                <td><?= $message['id'] ?></td>
                <td><?= $message['nom_complet'] ?></td>
                <td><?= $message['email'] ?></td>
                <td><?= $message['telephone'] ?></td>
                <td><?= $message['message'] ?></td>
            </tr>
        <?php endforeach; ?>

    </table>
    </div>
</body>

</html>
