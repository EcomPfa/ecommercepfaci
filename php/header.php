   <!-- Pied de page -->
   <!DOCTYPE HTML>
   <html>
    <head>
    <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Header</title>
        <link rel="icon" type="image/x-icon" href="images/logo.jpeg">
        <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css"
    />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  header {
    position:fixed;
    top: 0;
    left: 0;
    padding: 10px;
    z-index: 999; /* Assurez-vous que le z-index du header est supérieur aux autres éléments pour qu'il s'affiche correctement */
    
    width: 100%;
    background-color: #fff;
    display: flex;
    align-items:center;
    justify-content: space-between;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }
    
    .logo img {
      height: 70px;
      align-items: center;
  
    }
    
    .search-bar {
      display: flex;
      align-items: 20px;
      margin:10px;
    }
    
    .search-bar input[type="text"] {
      padding: 10px;
      border-radius: 20px;
      border: none;
      background-color: #f2f2f2;
      margin-right: 10px;
      font-size: 16px;
      width: 200px;
    }
    
    .search-bar button {
      padding: 10px 20px;
      border-radius: 20px;
      border: none;
      background-color: chocolate;
      color: #fff;
      cursor: pointer;
      font-size: 16px;
    }
    
    .header-buttons ul {
      list-style: none;
      margin: 0;
      padding: 0;
      display: flex;
    }
    
    .header-buttons li {
      margin-left: 5px;
    }
    
    .header-buttons a {
      color: #2c2c2c;
      text-decoration: none;
      font-size: 16px;
    }
    
    .fa {
      margin-right: 10px;
    }
    </style>
    </head>
    <body>
        <!-- En-tête -->
  <header>
    <!-- Barre de navigation -->
    <nav>
      <ul> 
        <li><a href="../pagecatgories/categories.php"><i class="fa-solid fa-bars"></i> Categories </a></li>
      </ul>
    </nav>

    <!-- Logo -->
    <div class="logo">
      <a href="page-acceuil.php"><img src="images/logo.jpeg" alt="Logo"></a> 

    </div>

    <!-- Barre de recherche -->
    <div class="search-bar">
      <form action="#" method="get">
        <input type="text" name="query" placeholder="Recherche..."> 
        <button type="submit"><i class="fa fa-search"></i></button>
      </form>
    </div>

    <!-- Boutons de connexion, inscription et panier -->
    <div class="header-buttons">
      <ul>
        <li><a class="dropdown-item" href="inscri-conn.php"><i class="fa fa-user"></i>Connexion</a></li>
        <li><a href="panier.php"><i class="fa fa-shopping-cart"></i> Panier</a></li>
      </ul>
    </div>
  </header>
</body>
</html>