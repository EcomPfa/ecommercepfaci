<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>connexion-inscription</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
<style>
    @import url('https://fonts.googleapis.com/css?family-Montserrat:400,800');


    .fa {
      margin-right: 10px;
    }

    
.my-select {
    height: 30px;
    width: 150px;
    text-align: center;
    font-size: 16px;
    }

* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}

body {
    background: #f6f5f7;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    font-family: 'Montserrat', sans-serif;
    height: 100vh;
}

.container {
    position: relative;
    overflow: hidden;
    min-height: 480px;
    width: 768px;
    max-width: 100%;
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 8px 24px chocolate,
                0 8px 8px chocolate;
}

.form-container {
    position: absolute;
    top: 0;
    height: 100%;
    transition:.6s ease-in-out;
}

.sign-up-container {
  left: 0;
  width: 50%; 
  opacity: 0;
  z-index:1; 
}

.container.panel-active .sign-up-container {
transform: translateX(100%);
opacity: 1;
z-index: 5;
animation: show .6s
}

@keyframes show {
  0%, 49.99%{
     opacity: 0;
     z-index: 1;
   }
  50%, 100%{
     opacity: 1;
     z-index: 5;
  }
}

.container.panel-active .login-container {
    transform: translateX(100%);
    }
    
.login-container {
left: 0;
width: 50%;
z-index: 2;
}

form {
    background-color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 50px;
    height: 100%;
    text-align: center;
}

input {
    background-color: #d8af6e;
    border: none;
    padding: 12px 15px;
    margin: 8px 0;
    width: 100%;
}
 button.ghost {
    background-color: transparent;
 }
button{
    background-color: #d8af6e;
    color: black;
    border: 1px solid #d8af6e;
    font-size: 16px;
    font-weight: bold;
    padding: 16px 32px;
    margin-top: 24px;
    letter-spacing: 1px;
    border-radius: 50px;
    border-color: chocolate;
}

button:hover {
    cursor: pointer;
    background-color: chocolate;
    color:#d8af6e;
}


button:active {
transform: scale(.95);
}

.social-container {
    margin: 24px 0;
}

.social-container a {
display: inline-flex;
justify-content: center;
align-items: center;
margin: 0 4px;
height: 40px;
width: 40px;
border-radius: 50%;
background-color: #d8af6e;
border: 1px solid chocolate;
}

h1 {
margin: 0;
font-size: 24px;
}

span {
font-size: 14px;
}

a{
    text-decoration: none;
    font-size: 14px;
    margin: 8px 0 16px;
    color: #333;
}

.overlay-container {
    position: absolute;
    top: 0;
    left: 50%;
    width: 50%;
    height: 100%;
    overflow: hidden;
    z-index: 100;
    transition: transform .6s ease-in-out;
}

.container.panel-active .overlay-container {
    transform: translateX(-100%) ;
}

.overlay {
    background: linear-gradient(to right, #d8af6e, #f6f5f7);
    background: #d8af6e;
    color: chocolate;
    position: relative;
    left: -100%;
    height: 100%;
    width: 200%;
    transform: translateX(0);
}

.container.panel-active .overlay {
    transform: translateX(50%);
}

.overlay-panel {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 40px;
    text-align: center;
    top: 0;
    height: 100%;
    width: 50%;
    transform: translateX(0);
    transition: transform .6s ease-in-out;
}

.overlay-left {
    transform: translateX(-20%);
}

.container.panel-active .overlay-left {
    transform: translateX(0);
}

.overlay-right {
    right: 0;
    transform: translateX(0);
}

.container.panel-active .overlay-right {
    transform: translateX(20%);
}

p {
    color: black;
    font-size: 14px;
    font-weight: 100;
    line-height: 20px;
    letter-spacing: .5px;
    margin: 20px 0 30px;
}
</style>
</head>


<body>
<?php include("header.php") ?>
  
 

    <div class="container" id="container">
        <div class="form-container sign-up-container">
       
            <form action="inscription.php" method="POST">
                <h1>Créer un compte</h1>
                <div class="social-container">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-google-plus-g"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
                <span>Ou utiliser votre compte gmail</span>
                <input type="text" name="nom_complet"  name="nom_complet" placeholder="Nom complet" required/>
                <input type="email" name="email" placeholder="E-mail" required/>
                <input type="password" name="mdp" placeholder="Mot de passe" required/><br>
                <label>Je suis:</label>
                <select class="my-select" name="role"  required>
                        <option value=" "></option>
                          <option value="client">Client</option>
                          <option value="vendeur">Vendeur</option>
                      </select>
              
               <script>// Assume the selectedRole variable holds the desired value ('client' or 'vendeur')
                    const selectedRole = 'client'; // Example value
                    
                    // Get the <select> element
                    const roleSelect = document.getElementById('role');
                    
                    // Set the selected option based on the selectedRole value
                    roleSelect.value = selectedRole;
                </script>
                <button action="inscription.php" name="boutton">S'INSCRIRE</button>
            </form>
            </div>
            <?php
if (isset($_POST['inscription_success'])) {
    ?>
    <div class="inscription-reussie">
        <h1>Inscription réussie</h1>
        <p>Votre inscription a été effectuée avec succès.</p>
        <div class="icone-vrai"> 
            <i class="fas fa-check-circle"></i>
        </div>
    </div>
    <?php
}
?>
<style>
.inscription-reussie {
    background-color: #e6f2e6; /* Couleur de fond verte */
    padding: 20px;
    border-radius: 5px;
    text-align: center;
}

.inscription-reussie h1 {
    color: #006600; /* Couleur du texte en vert foncé */
}

.inscription-reussie p {
    color: #008000; /* Couleur du texte en vert */
}

.icone-vrai {
    font-size: 50px;
    color: #008000; /* Couleur de l'icône en vert */
    margin-top: 20px;
}

/* Utilisez la classe "fa-check-circle" pour l'icône de vérification, assurez-vous d'inclure la bibliothèque Font Awesome */
</style>

        <div class="form-container login-container">
     
            <form action="connexion.php" method="POST">
                <h1>Se connecter</h1>
                <div class="social-container">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-google-plus-g"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
                <span>Entrer vos informations</span>
                <input type="email" name="email" placeholder="E-mail" required/>
                <input type="mdp" name="mdp" placeholder="Mot de passe" required/><br>
                <button name="boutton">SE CONNECTER</button>
            </form>
        </div>

        <div class="overlay-container">
            <div class="overlay">
              <div class="overlay-panel overlay-left">
                  <h1>Bienvenue de nouveau!</h1>
                  <p>Pour rester connecté avec nous, connectez-vous avec vos informations personnelles</p>
                  <button name="boutton" class="ghost" id="login">SE CONNECTER</button>
              </div>
              <div class="overlay-panel overlay-right">
                  <h1>Bienvenue!</h1>
                  <p>Créer votre compte</p>
                  <button class="ghost" id="signUp" name="boutton">S'INSCRIRE</button>
             </div>
          </div>   
        </div>
    </div>
    <?php
if (isset($_GET['login_err'])) {
    $err = $_GET['login_err'];

    switch ($err) {
        case 'mdp':
            echo '<div class="alert alert-danger">Mot de passe incorrect.</div>';
            break;

        case 'email':
            echo '<div class="alert alert-danger">Adresse e-mail incorrecte.</div>';
            break;
    }
}
?>

    <script src="https://kit.fontawesome.com/your-key.js"></script>
    <script>
        const container = document.getElementById('container');
const signUpButton = document.getElementById('signUp');
const loginButton = document.getElementById('login');

signUpButton.addEventListener('click', () => {
    container.classList.add('panel-active');
});

loginButton.addEventListener('click', () => {
    container.classList.remove('panel-active');
});
    </script>

</body>
</html>
