<?php
$server = "localhost";
$user = "root";
$password = "";
$database = "ecommerce";

// Connexion à la base de données
$conn = mysqli_connect($server, $user, $password, $database);

// Vérification de la connexion
if (!$conn) {
    die("Erreur de connexion à la base de données : " . mysqli_connect_error());
}

// Variable pour afficher le message de succès
$message = "";

// Traitement de la soumission du formulaire
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupération des valeurs du formulaire
    $compte = $_POST['compte'];
    $activation = $_POST['activation'];

    // Mise à jour du compte dans la base de données
    $query = "";

    if ($activation === 'activer') {
        $query = "UPDATE clients SET compte_active = 1 WHERE id = $compte";
    } else if ($activation === 'desactiver') {
        $query = "UPDATE clients SET compte_active = 0 WHERE id = $compte";
    }

    // Exécution de la requête de mise à jour
    if (!empty($query)) {
        $result = mysqli_query($conn, $query);
        if (!$result) {
            echo "Erreur de mise à jour du compte : " . mysqli_error($conn);
        } else {
            $message = "Le compte a été mis à jour avec succès.";
        }
    }
}
?>

<?php include("sidebar-header.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <title>compte activation</title>
    
    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }
      
        body {
            background: #f6f5f7;
            font-family: 'Montserrat', sans-serif;
            height: 100vh;
        }
        .container {
    overflow: hidden;
    width: 60%;
    height: auto;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 8px 24px chocolate, 0 8px 8px chocolate;
    margin: 0 auto;
    margin-top: 200px;
    padding: 20px;
}

.content-container {
    margin-left: 250px; /* Largeur de la sidebar */
    margin-right: 0;
}


        h2 {
            text-align: center;
            padding: 20px;
            color: chocolate;
        }

        form {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 20px;
        }

        label {
            margin-bottom: 10px;
        }

        select, input[type="submit"] {
            width: 70%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        input[type="submit"] {
            background-color: #d8af6e;
            color: black;
            font-weight: bold;
            letter-spacing: 1px;
            border-color: chocolate;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: chocolate;
            color: #d8af6e;
        }

        .message {
            background-color: #dff0d8;
            color: #3c763d;
            padding: 10px;
            text-align: center;
            display: none;
        }
    </style>
</head>
<body>

<div class="content-container">
        <div class="container" id="container">
    <form action="admin-comptes.php" method="post">
        <h2>Les comptes des utilisateurs</h2>
        <label for="compte">Sélectionnez un compte :</label>
        <select name="compte" id="compte">
            <option> </option>
            <!-- Remplir le select avec les comptes de la table des utilisateurs -->
            <?php
            // Sélection des comptes de la table "clients" et "vendeurs"
            $query = "SELECT id, nom_complet FROM clients UNION SELECT id, nom_complet FROM vendeurs";
            $result = mysqli_query($conn, $query);

            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    echo "<option value='" . $row['id'] . "'>" . $row['nom_complet'] . "</option>";
                }
            }
            ?>
        </select>
        <br>
        <label for="activation">Activer ou désactiver le compte :</label>
        <select name="activation" id="activation">
            <option> </option>
            <option value="activer">Activer</option>
            <option value="desactiver">Désactiver</option>
        </select>
        <br>
        <input type="submit" value="Soumettre">
    </form>
          </div>
    <div id="message" class="message"></div>
</div>

<?php if (!empty($message)) { ?>
    <script>
        // Afficher le message de succès et le faire disparaître après 5 secondes
        var messageElement = document.getElementById('message');
        messageElement.innerText = '<?php echo $message; ?>';
        messageElement.style.display = 'block';
        setTimeout(function() {
            messageElement.style.display = 'none';
        }, 3000);
    </script>
<?php } ?>

</body>
</html>

