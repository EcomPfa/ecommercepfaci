<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Récupérer les données du formulaire
    $nom_complet = $_POST['nom_complet'];
    $email = $_POST['email'];
    $mdp = $_POST['mdp'];
    $role = $_POST['role'];

    // Effectuer des vérifications supplémentaires si nécessaire

    // Connexion à la base de données
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ecommerce";

    $conn = new mysqli($servername, $username, $password, $dbname);

    // Vérifier la connexion
    if ($conn->connect_error) {
        die("Échec de la connexion : " . $conn->connect_error);
    }
    
    // Vérifier l'unicité de l'e-mail dans les tables clients, vendeurs et administrateur
    $query = "SELECT email FROM clients WHERE email = '$email'
              UNION
              SELECT email FROM vendeurs WHERE email = '$email'
              UNION
              SELECT email FROM administrateur WHERE email = '$email'";
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
        // L'e-mail existe déjà dans l'une des tables
        header('Location: inscription.php?email_err=exists');
        exit();
    }
    
    // Insérer les données dans la table appropriée en fonction du rôle
    if ($role === 'client') {
        $sql = "INSERT INTO clients (nom_complet, email, mdp) VALUES ('$nom_complet', '$email', '$mdp')";
    } elseif ($role === 'vendeur') {
        $sql = "INSERT INTO vendeurs (nom_complet, email, mdp) VALUES ('$nom_complet', '$email', '$mdp')";
    } else {
        // Rôle non valide, gérer l'erreur appropriée
        die("Rôle non valide");
    }

    if ($conn->query($sql) === TRUE) { 
        if (!empty($_POST['role'])) {
            switch ($_POST['role']) {
                case "client":
                    header("Location: inscri-conn.php");
                    exit;
                case "vendeur":
                    header("Location: inscri-conn.php");
                    exit;
            }
        }
        exit(); 
    } else {
        echo "Erreur lors de l'inscription : " . $conn->error;
        header('Location: inscri-conn.php');
        exit();
    }
}

// Fermer la connexion
$conn->close();
?>
