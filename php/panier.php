<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
    <link rel="stylesheet" href="panier.css">
    <link rel="stylesheet" href="page-acceuil.css">

    <style>
       @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css');

table {
  width: 100%;
  border-collapse: collapse;
}
th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #ddd;
  height: max-content;
}

thead th {
  background-color: #f2f2f2;
  height: 40px;
}
h2 .pan{
    font-size: 2px;
}
.action-icon {
  display: inline-block;
  margin-right: 10px;
}

.action-icon i {
  margin-right: 5px;
}
.button-container {
    text-align: right; /* Aligner le contenu à droite */
  }
  
  .button {
    height: max-content;
    background-color: chocolate;
    color: white;
    font-size: 15px;
    border: none;
    padding: 10px 20px;
    border-radius: 5px;
    outline: none;
    cursor: pointer;
    transition: transform 0.5s;
  }
  
  input[type="text"][name="query"] {
    padding: 10px;
    width: 300px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  
  input[type="text"][name="query"]:focus {
    outline: none;
    border-color: #d2691e;
  }
  h1{
     color: chocolate;
     text-align: center;
  }
  .quantity-container {
  display: flex;
  align-items: center;
}

.quantity-btn {
  width: 30px;
  height: 30px;
  background-color: #f2f2f2;
  border: none;
  text-align: center;
  cursor: pointer;
  font-weight: bold;
  font-size: 16px;
}

.quantity-input {
  width: 30px;
  height: 30px;
  text-align: center;
  border: none;
  background-color: #f2f2f2;
  font-weight: bold;
  font-size: 16px;
}

    </style>
</head>
<body class="panier">
<?php include("header.php"); ?>
<br><br><br> <br> <br> <br> <br> <br> 
<span class="pan"><h2>Votre panier</h2><br></span>
<section>
    <table>
        <thead>
        <tr>
            <th>PRODUIT</th>
            <th>EDITER</th>
            <th>TOTAUX</th>
        </tr>
       </thead>
       <tbody>
        <tr>
        


        </tr>
       </tbody>
        <script>
            // Sélection des éléments HTML
            const minusBtns = document.querySelectorAll('.minus-btn');
            const plusBtns = document.querySelectorAll('.plus-btn');
            const quantityInputs = document.querySelectorAll('.quantity-input');

            // Gestionnaire d'événement pour le bouton "-"
            minusBtns.forEach(function(minusBtn) {
                minusBtn.addEventListener('click', function() {
                    let quantityInput = this.parentNode.querySelector('.quantity-input');
                    let currentValue = parseInt(quantityInput.value);
                    if (currentValue > 1) {
                        quantityInput.value = currentValue - 1;
                    }
                });
            });

            // Gestionnaire d'événement pour le bouton "+"
            plusBtns.forEach(function(plusBtn) {
                plusBtn.addEventListener('click', function() {
                    let quantityInput = this.parentNode.querySelector('.quantity-input');
                    let currentValue = parseInt(quantityInput.value);
                    quantityInput.value = currentValue + 1;
                });
            });
        </script>
        <!-- Ajoutez d'autres lignes ici -->
        <thead>
        <th></th>
        <th></th>
        <th class="tot">Total de la commande: </th>
        </thead>
        </tbody>
    </table>
    <br><br>
    <a href="commander.php">
        <div class="button-container">
            <button class="button">COMMANDER</button>
        </div>
    </a>

</section>
<br> <br> <br> <br> <br> <br> 
<?php include("footer.php"); ?>
</body>
</html>
