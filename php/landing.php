<?php

    // Connexion à la base de données
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ecommerce";

    $conn = new mysqli($servername, $username, $password, $dbname);

    // Vérifier la connexion
    if ($conn->connect_error) {
        die("Échec de la connexion : " . $conn->connect_error);
    }
?>
<!doctype html>
<html lang="en">

<head>
    <title>Espace membre client</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
    .container {
        background-color: #fff;
        border-radius: 10px;
        box-shadow: 0 8px 24px chocolate, 0 8px 8px chocolate;
        padding: 20px;
        max-width: 768px;
        margin: 0 auto;
        margin-top: 100px;
    }

    .container h1 {
        text-align: center;
        margin-bottom: 10px;
    }

    .container form {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .container form label {
        margin-top: 10px;
    }


    .container form button[type="submit"] {
        background-color: chocolate;
        color: black;
        border: none;
        font-size: 16px;
        font-weight: bold;
        padding: 12px 24px;
        margin-top: 20px;
        border-radius: 5px;
        cursor: pointer;
    }

    .container form button[type="submit"]:hover {
        background-color: chocolate;
        color: #d8af6e;
    }

    .container form button[type="submit"]:focus {
        outline: none;
        background-color: #d8af6e;
    }

    .container form button[type="submit"]:active {
        transform: scale(0.95);
       background-color: #d8af6e;
    }

    .container form button.btn-info,
    .container form button.btn-danger {
        background-color: chocolate;
        color: white;
        border: none;
        font-size: 16px;
        font-weight: bold;
        padding: 12px 24px;
        margin-top: 20px;
        border-radius: 5px;
        cursor: pointer;
        width: 100%;
    }

    .container form button.btn-danger:hover {
        background-color: chocolate;
    }

    .container form button.btn-danger:focus {
        outline: none;
    }

    .container form button.btn-danger:active {
        transform: scale(0.95);
    }

.profile-form {
   height: 100%;
}

    input{
    background-color: #d8af6e;
    border: none;
    padding: 12px 15px;
    margin: 8px 0;
    width: 70%;
    }


.search-bar input[type="text"] {
    background-color: white;
    border: none;
    padding: 12px 15px;
    margin: 8px 0;
    width: 100%;
    color: black;
}

.search-bar button[type="submit"] {
    background-color: chocolate;
    color: white;
    border: none;
    padding: 12px 20px;
    margin: 8px 0;
    cursor: pointer;
}

.search-bar button[type="submit"]:hover {
    background-color: #8b4513;
}

</style>
</head>
<body>

<div class="container">
    <div class="col-md-12">
        <div class="text-center">
            <h1 class="p-5">Espace membre client:</h1>
            <hr />
            <!-- Formulaire de modification du profil -->
            <form class="profile-form" action="landing.php" method="POST">
                <input type="text" name="nom_complet" id="nomComplet" placeholder="Nom complet " >
                <input type="text" name="ville" id="ville" placeholder="Ville " >             
                <input type="tel" name="telephone" id="telephone" placeholder="Téléphone " >
                <input type="password" name="mdp" id="mdp" placeholder="Ancien mot de passe " >
                <input type="password" name="mdp" id="mdp" placeholder="Nouveau mot de passe " >
                <button type="submit" class="btn btn-primary">Mettre à jour le profil</button>
            </form><br>
        </div>

        <?php if (isset($successMessage)): ?>
        <div class="alert alert-success mt-3"><?php echo $successMessage; ?></div>
        <?php endif; ?>
    </div>
</div>



<!-- Bootstrap JS and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popper
js/core@2.5.4/dist/umd/popper.min.js"
integrity="sha384-R9aRtbnP8z4QP7pZ4c45DeHbD0z9VVLZ//PxT8BgO1QtlrW/iUudcWXrQbE4iz8q"
crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
     integrity="sha384-pzjw8f+ua6d5qk5t5lTR12E9xjmvt5URPB6TPbXN2rFMs0AgFETwWoXdhiDqO4x"
     crossorigin="anonymous"></script>

</body>
</html>