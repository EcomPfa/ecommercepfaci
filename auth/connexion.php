<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start(); // Démarrer la session
$_SESSION['vendeur_id'] = null;
$_SESSION['client_nom'] = null;
$_SESSION['client_id'] = null;
$_SESSION["loggedin"] = null;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Récupérer les données du formulaire
    $email = $_POST['email'];
    $mdp = $_POST['mdp'];

    include_once('../connection_db.php');

    // Vérifier si les informations de connexion correspondent à un administrateur
    $sql_admin = "SELECT email FROM administrateur WHERE email = ? AND mdp = ?";
    $stmt_admin = $conn->prepare($sql_admin);
    $stmt_admin->bind_param("ss", $email, $mdp);
    $stmt_admin->execute();
    $result_admin = $stmt_admin->get_result();

    if ($result_admin !== false && $result_admin->num_rows > 0) {
        // Les informations de connexion sont correctes pour un administrateur
        header("Location: ../dashboard/admin.php");
        exit();
    }

    // Vérifier les informations de connexion dans les tables clients et vendeurs
    $sql = "SELECT email, mdp, role FROM clients WHERE email = ? AND mdp = ? UNION SELECT email, mdp, role FROM vendeurs WHERE email = ? AND mdp = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssss", $email, $mdp, $email, $mdp);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result !== false && $result->num_rows > 0) {
        $data = $result->fetch_assoc();
        $email = $data['email'];
        $mdp = $data['mdp'];

        // Check if the user is a client
        $sql_clients = "SELECT id, nom_complet, role FROM clients WHERE email = ?";
        $stmt_clients = $conn->prepare($sql_clients);
        $stmt_clients->bind_param("s", $email);
        $stmt_clients->execute();
        $result_clients = $stmt_clients->get_result();

        if ($result_clients !== false && $result_clients->num_rows > 0) {
            $res = $result_clients->fetch_assoc();
            $role = $res['role'];
            $nom_complet = $res['nom_complet'];
            $id = $res['id'];

            if ($role === 'client') {
                $_SESSION['loggedin'] = true;
                $_SESSION['client_id'] = $id;
                $_SESSION['client_nom'] = $nom_complet;

                echo "Redirecting to index.php"; // Debug statement
                header("Location: ../index.php");
                exit();
            }
        }

        // Check if the user is a vendor
        $sql_vendeurs = "SELECT id, role FROM vendeurs WHERE email = ?";
        $stmt_vendeurs = $conn->prepare($sql_vendeurs);
        $stmt_vendeurs->bind_param("s", $email);
        $stmt_vendeurs->execute();
        $result_vendeurs = $stmt_vendeurs->get_result();

        if ($result_vendeurs !== false && $result_vendeurs->num_rows > 0) {
            $data = $result_vendeurs->fetch_assoc();
            $role = $data['role'];
            $id = $data['id'];

            if ($role === 'vendeur') {
                $_SESSION['vendeur_id'] = $id;
                echo "Redirecting to ../dashboard/vendeur.php"; // Debug statement
                header("Location: ../dashboard/vendeur.php");
                exit();
            }
        }
    } else {
        // Les informations de connexion sont incorrectes
        // Rediriger vers la page d'inscription/connexion
        echo "Redirecting to inscri-conn.php"; // Debug statement
        header("Location: inscri-conn.php");
        exit();
    }

    // Fermer la connexion
    $conn->close();
}
?>