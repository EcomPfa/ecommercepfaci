<?php include("../layouts/header.php") ?>


<section class="section-inscri">
    <div class="container" id="container">
        <div class="form-container sign-up-container">

            <form action="inscription.php" method="POST">
                <h1>Créer un compte</h1>
                <div class="social-container">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-google-plus-g"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
                <span>Ou utiliser votre compte gmail</span>
                <input type="text" name="nom_complet" name="nom_complet" placeholder="Nom complet" required />
                <input type="email" name="email" placeholder="E-mail" required />
                <input type="password" name="mdp" placeholder="Mot de passe" required /><br>
                <label>Je suis:</label>
                <select class="my-select" name="role" required>
                    <option value=""></option>
                    <option value="client">Client</option>
                    <option value="vendeur">Vendeur</option>
                </select>

                <script>// Assume the selectedRole variable holds the desired value ('client' or 'vendeur')
                    const selectedRole = 'client'; // Example value

                    // Get the <select> element
                    const roleSelect = document.getElementById('role');

                    // Set the selected option based on the selectedRole value
                    roleSelect.value = selectedRole;
                </script>
                <button action="inscription.php" name="boutton">S'INSCRIRE</button>
            </form>
        </div>

        <?php
        if (isset($_POST['inscription_success'])) {
            ?>
            <div class="inscription-reussie">
                <h1>Inscription réussie</h1>
                <p>Votre inscription a été effectuée avec succès.</p>
                <div class="icone-vrai">
                    <i class="fas fa-check-circle"></i>
                </div>
            </div>
            <?php
        }
        ?>

        <div class="form-container login-container">
            <form action="connexion.php" method="POST">
                <h1>Se connecter</h1>
                <div class="social-container">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-google-plus-g"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
                <span>Entrer vos informations</span>
                <input type="email" name="email" placeholder="E-mail" required />
                <input type="mdp" name="mdp" placeholder="Mot de passe" required /><br>
                <button name="boutton">SE CONNECTER</button>
            </form>
        </div>

        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-left">
                    <h1>Bienvenue de nouveau!</h1>
                    <p>Pour rester connecté avec nous, connectez-vous avec vos informations personnelles</p>
                    <button name="boutton" class="ghost" id="login">SE CONNECTER</button>
                </div>
                <div class="overlay-panel overlay-right">
                    <h1>Bienvenue!</h1>
                    <p>Créer votre compte</p>
                    <button class="ghost" id="signUp" name="boutton">S'INSCRIRE</button>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_GET['login_err'])) {
    $err = $_GET['login_err'];

    switch ($err) {
        case 'mdp':
            echo '<div class="alert alert-danger">Mot de passe incorrect.</div>';
            break;

        case 'email':
            echo '<div class="alert alert-danger">Adresse e-mail incorrecte.</div>';
            break;
    }
}
?>

<?php include("../layouts/footer.php") ?>