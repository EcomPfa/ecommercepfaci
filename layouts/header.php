<?php 
    $base_url="http://localhost/ecommerce/";
    session_start();
    $nom_client = $_SESSION["client_nom"]?? null;
    $client_id = $_SESSION["client_id"]?? null;
?>

<!-- Pied de page -->
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Header</title>
  <link rel="icon" type="image/x-icon" href="images/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
    integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />

  <link rel="stylesheet" href="<?php echo $base_url ?>assets/style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>

  <!-- En-tête -->
  <header>
    <!-- Barre de navigation -->
    <a href="<?php echo $base_url ?>pages/categories_produits/client_categories.php">
      <i class="fa-solid fa-bars"></i> Categories 
    </a>

    <!-- Logo -->
    <div class="logo">
      <a href="<?php echo $base_url ?>">
        <img src="<?php echo $base_url ?>assets/images/logo.png" alt="Logo">
      </a>
    </div>

    <!-- Barre de recherche -->
    <div class="search-bar">
      <form action="<?php echo $base_url ?>pages/produits/client_produits.php" method="get">
        <input type="text" name="query" placeholder="Recherche...">
        <button type="submit"><i class="fa fa-search"></i></button>
      </form>
    </div>

    <!-- Boutons de connexion, inscription et panier -->
    <div class="header-buttons">
      <ul>
        

        <?php 
            if($nom_client == null){ ?>
              <li>
                <a class="dropdown-item" href="<?php echo $base_url ?>auth/inscri-conn.php">
                  <i class="fa fa-user"></i>Connexion
                </a>
              </li>
            <?php  } ?>

          <?php if($nom_client != null){ ?>
          <li>
            <a href="<?php echo $base_url ?>pages/clients/edit/edit_client.php?id= <?php echo $client_id ?>">
               <i class="fa fa-user"></i> Modifier le profil
            </a>
          </li>
          <li>
            <a href="<?php echo $base_url ?>pages/panier/panier.php">
              <i class="fa fa-shopping-cart"></i> Votre panier
            </a>
          </li>
          <li>
            <a href="<?php echo $base_url ?>pages/panier/produits/list.php">
              <i class="fa fa-shopping-cart"></i> Vos produits
            </a>
          </li>
          
              <li>
                <a href="<?php echo $base_url ?>auth/logout.php">
                   <i class="fa fa-user"></i> Déconnecter
                </a>
              </li>
          <?php  } ?>
          
      </ul>
    </div>
  </header>