<?php
$base_url = "http://localhost/ecommerce/";
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Admin Dashboard</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      integrity="sha512-xxxx" crossorigin="anonymous" />
  
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <!-- Montserrat Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet">
  
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/style_admin.css">

</head>

<body>
  <div class="grid-container">

    <!-- Header -->
    <header class="header">
      <div class="menu-icon" onclick="openSidebar()">
        <span class="material-icons-outlined">menu</span>
      </div>
      <div class="header-left"></div>
      <div class="header-right">
          <a href="<?php echo $base_url ?>auth/logout.php" style="text-decoration: blink;" class="text-dark">
              <i class="fa fa-user"></i> Déconnecter
          </a>
      </div>
    </header>
    <!-- End Header -->

    <!-- Sidebar -->
    <aside id="sidebar">
      <div class="sidebar-title">
        <div class="sidebar-brand">
          <span class="material-icons-outlined"></span>
          <a href="<?php echo $base_url ?>" class="active-link">OLIVIA</a>
        </div>
        <span class="material-icons-outlined" onclick="closeSidebar()">close</span>
      </div>

      <ul class="sidebar-list">
        <li class="sidebar-list-item">
          <a href="<?php echo $base_url ?>dashboard/admin.php">
            <span class="material-icons-outlined">dashboard</span>
            dashboard
          </a>
        </li>
        <li class="sidebar-list-item">
          <a href="<?php echo $base_url ?>pages/categories_produits/admin_categories.php">
            <span class="material-icons-outlined">inventory_2</span>
            Categories
          </a>
        </li>
        <li class="sidebar-list-item">
          <a href="<?php echo $base_url ?>pages/produits/admin_produits.php">
            <span class="material-icons-outlined">inventory_2</span>
            Produits
          </a>
        </li>
        <li class="sidebar-list-item">
          <a href="<?php echo $base_url ?>pages/clients/admin_clients.php">
            <span class="material-icons-outlined">groups_2</span>
            Clients
          </a>
        </li>
        <li class="sidebar-list-item">
          <a href="<?php echo $base_url ?>pages/clients/comptes.php">
            <span class="material-icons-outlined">person</span> Activation comptes
          </a>
        </li>
        <li class="sidebar-list-item">
          <a href="<?php echo $base_url ?>pages/vendeurs/admin_vendeurs.php">
             <span class="material-icons-outlined">groups_2</span> Vendeurs
          </a>
        </li>
        <li class="sidebar-list-item">
          <a href="<?php echo $base_url ?>pages/ordres/commandes.php">
            <i class="fas fa-boxing"></i>
            <span class="material-icons-outlined">inventory_2</span> Ordres
          </a>
        </li>
        <!-- <li class="sidebar-list-item">
          <a href="admin-messages.php">
            <span class="material-icons-outlined">messages</span>Messages
          </a>
        </li> -->

      </ul>
    </aside>
    <!-- End Sidebar -->