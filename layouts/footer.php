<?php 
    $base_url="http://localhost/ecommerce/";
?>
<!-- footer -->
<footer class="foot_sec">
  <div class="foot_top">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="foot_left">
            <img src="<?php echo $base_url ?>assets/images/logo.png" alt="">
            <p>
            Découvrez notre sélection de produits du terroir, soigneusement choisis pour vous offrir une expérience authentique et savoureuse. 
            Soutenez les producteurs locaux en faisant vos achats dans notre boutique.
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="foot_rt">
            <h3>CONTACTS</h3>
            <p>
              <span>Address: </span>
              Agdal, Rabat-Morocco
            </p>
            <p>
              <span>Phone: </span>
              <a href="tel:#">+212-677889970</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="foot_btm">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p><a href="#"> olivia</a> © 2023. Allright Reserved.</p>
        </div>
        <div class="col-md-6">
          <p>Design and Development By <a href="#"> kkk kkk</a></p>
        </div>
      </div>
    </div>
  </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/your-key.js"></script>
<script src="http://localhost/ecommerce/assets/script.js"></script>
</body>

</html>