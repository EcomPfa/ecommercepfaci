

const container = document.getElementById('container');
const signUpButton = document.getElementById('signUp');
const loginButton = document.getElementById('login');

signUpButton.addEventListener('click', () => {
    container.classList.add('panel-active');
});

loginButton.addEventListener('click', () => {
    container.classList.remove('panel-active');
});

/** ** ** ** **/
function show_text(){
    document.getElementById('commentaire_etoile').style.display ='none';
    document.getElementById('commentaire_img').style.display ='none';
    document.getElementById('commentaire_text').style.display = 'block';
}

function show_image(){
    document.getElementById('commentaire_text').style.display ='none';
    document.getElementById('commentaire_etoile').style.display ='none';
    document.getElementById('commentaire_img').style.display = 'block';
}

function show_etoile(){
    document.getElementById('commentaire_text').style.display ='none';
    document.getElementById('commentaire_img').style.display ='none';
    document.getElementById('commentaire_etoile').style.display = 'block';
}
